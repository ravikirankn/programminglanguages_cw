/*
 * groovy can provide both static typing and dynamic typing.
 * its static typing is weak though. people call it optional typing.
 * its more toward dynamic typing.
 * 
 *in static methods typing is required. in non static methods typing is not required in grrovy.
 *
 *java and groovy compile the code into byte code, groovy is lenient.
 *
 *so we now add the method to the string class.
 *
 *this is not the power of dynamic typing, this is power of meta programming
 *
 */


greet = "hello"
println greet
try{
  println greet.shout()
}catch(Exception e){
  println "oops exception caught"
}
String.metaClass.shout = {
  ->
  delegate.toUpperCase()
}
println greet.shout()