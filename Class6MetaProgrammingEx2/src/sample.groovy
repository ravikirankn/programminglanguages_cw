/*
 * this is not code synthesis but this is code injection.
 * 
 * we can also add methods dynamically, ex: sample1.groovy
 */
str = "hello"
println str
println str.class

String.metaClass.shout = {->
  delegate.toUpperCase()
}
println str.shout()