playersAndScores = [:]
def methodMissing(String name,args){
  if(args.size()==1){
    playersAndScores[name] = args[0]
  }
}

def getWinner(){
  def winner = ""
  def maxScore = 0;
  playersAndScores.each {name,score->
    if(maxScore<score){
      maxScore = score
      winner = name
    }
  }
  println "The winner is $winner with score $maxScore"
}

