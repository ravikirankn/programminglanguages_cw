package main.scala.webcrawlerThreadsBased

import main.scala.objects.WebCrawler

class CrawlerThread(subUrl: String, crawler: WebCrawlerMultiThreaded) extends Runnable {

  def run() {
    try {
      val links = WebCrawler.getSubUrlsFromUrl(subUrl)
      crawler.linksQueue.addAll(scala.collection.JavaConversions.asJavaCollection(links))
      if (crawler.barrier.getNumberWaiting() == 1) {
        crawler.barrier.await();
        println("awaiting")
      }
    } catch {
      case e: Exception => println("Exception in connecting to url: " + subUrl)
    }
  }

}