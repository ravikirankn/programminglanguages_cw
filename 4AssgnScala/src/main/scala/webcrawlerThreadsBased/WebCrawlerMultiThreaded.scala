package main.scala.webcrawlerThreadsBased

import java.util.concurrent.CyclicBarrier
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.LinkedBlockingQueue
import scala.collection.mutable.Set
import java.util.concurrent.TimeUnit

class WebCrawlerMultiThreaded(url: String) {

  private var crawlService: ExecutorService = null
  private var visitedLinks = Set[String]()
  var linksQueue = new LinkedBlockingQueue[String]()
  val barrier = new CyclicBarrier(2);

  def crawl() = {
    crawlService = Executors.newFixedThreadPool(100)
    var timeBefore = System.currentTimeMillis();
    linksQueue.add(url)
    while (!linksQueue.isEmpty()) {
      val nextUrl = linksQueue.remove()
      if (!visitedLinks.contains(nextUrl)) {
        println(linksQueue.size())
        visitedLinks.add(nextUrl)
        try {
          val crawlJob = new CrawlerThread(nextUrl, this);
          crawlService.submit(crawlJob);
          if (linksQueue.isEmpty()) {
            barrier.await();
          }
        } catch {
          case e: Exception => println("Error crawling URL: " + nextUrl + e)
        }
      }
    }
    crawlService.shutdown();
    crawlService.awaitTermination(Long.MaxValue, TimeUnit.NANOSECONDS);
    visitedLinks
  }

}