package main.scala

import akka.actor.ActorSystem
import main.scala.webcrawlerActorBased.MasterActor
import akka.actor.Props
import main.scala.webcrawler.WebCrawlerSequential

object MainProgram {

  def main(args: Array[String]) {
    val crawler = new WebCrawlerSequential
    val url = "http://www2.cs.uh.edu/~svenkat/fall2014pl/samplepages"

    println(s"Start url: $url")
    val stats = timeTaken { crawler.getLinksFromSite(url) }
    println(s"Sequential: ${stats._1.size} links, took ${stats._2} msecs")

    val system = ActorSystem("MySystem")
    val myActor = system.actorOf(Props[MasterActor], name = "masterActor")
    myActor ! url
  }

  def timeTaken[T](block: => T): (T, Long) = {
    val timeBefore = System.currentTimeMillis
    val returnValueOfBlock = block
    val timeAfter = System.currentTimeMillis
    (returnValueOfBlock, timeAfter - timeBefore)
  }

}
