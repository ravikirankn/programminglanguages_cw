package main.scala.webcrawler

import scala.collection.mutable.Set
import main.scala.objects.WebCrawler

class WebCrawlerSequential {
  val visitedLinks = Set[String]()

  def getLinksFromSite(url: String): Set[String] = {
    val subLinks = WebCrawler.getSubUrlsFromUrl(url)
    visitedLinks.add(url)
    subLinks.foreach(subUrl => {
      if (!subUrl.isEmpty() && !visitedLinks.contains(subUrl) && WebCrawler.breakingCondition(url, subUrl)) {
        getLinksFromSite(subUrl)
      }
    })
    visitedLinks
  }

}