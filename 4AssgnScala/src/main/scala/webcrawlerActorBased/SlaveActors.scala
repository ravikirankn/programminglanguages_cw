package main.scala.webcrawlerActorBased

import akka.actor.Actor
import main.scala.objects.WebCrawler

class SlaveActors extends Actor {

  def receive = {
    case subUrl: String => {
      try {
        val links = WebCrawler.getSubUrlsFromUrl(subUrl)
        sender ! links
      } catch {
        case e: Exception => println("Couldn't connect to url: " + subUrl)
      }
    }
    case _ ⇒ println("received unknown message")
  }

}