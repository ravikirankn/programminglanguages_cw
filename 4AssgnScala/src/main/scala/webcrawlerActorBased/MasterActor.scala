package main.scala.webcrawlerActorBased

import akka.actor.Actor
import scala.collection.mutable.Set
import akka.actor.Props
import main.scala.objects.WebCrawler
import akka.routing.RoundRobinPool

class MasterActor extends Actor {
  private var visitedLinks = Set[String]()
  private var linksRetrieved = 0
  private var timeBefore = 0.0

  def receive = {
    case url: String => {
      timeBefore = System.currentTimeMillis
      sendMessageToSlaves(url)
    }
    case links: Set[_] ⇒ {
      linksRetrieved += 1
      links.foreach(subUrl => {
        if (!subUrl.toString().isEmpty() && !visitedLinks.contains(subUrl.toString())) {
          sendMessageToSlaves(subUrl.toString())
        }
      })
      if (linksRetrieved == visitedLinks.size) {
        context.system.shutdown()
        println(s"Actor Based: ${visitedLinks.size} links, took ${System.currentTimeMillis - timeBefore} msecs")
      }
    }
    case _ ⇒ println("received unknown message")
  }

  def sendMessageToSlaves(url: String) {
    visitedLinks.add(url)
    val slaveActor = context.actorOf(RoundRobinPool(5).props(Props[SlaveActors]))
    slaveActor ! url
  }

}
