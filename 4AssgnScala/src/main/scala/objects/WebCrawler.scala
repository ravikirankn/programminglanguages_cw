package main.scala.objects

import org.jsoup.Jsoup
import java.net.URI
import org.jsoup.select.Elements
import org.jsoup.nodes.Element
import java.util.function.Consumer
import scala.collection.mutable.Set

object WebCrawler {

  def getSubUrlsFromUrl(url: String): Set[String] = {
    val links = getElements(url)
    var subLinks = Set[String]()
    links.forEach(new Consumer[Element]() {
      def accept(href: Element) {
        subLinks.add(href.attr("abs:href"))
      }
    })
    subLinks
  }

  def getElements(url: String): Elements = {
    val response = Jsoup.connect(url).ignoreContentType(true).ignoreHttpErrors(true).execute();
    if (response.statusCode() == 200) response.parse().select("a[href]")
    else new Elements
  }

  def breakingCondition(url: String, subUrl: String) = {
    new URI(url).getHost().equals(new URI(subUrl).getHost())
  }

}