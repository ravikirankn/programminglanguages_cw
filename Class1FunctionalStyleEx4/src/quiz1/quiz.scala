package quiz1

/*
 * 
 */
object quiz {

  class HelloWorld(val message: String) {
    def SayHello() = println(message)
  }

  def main(args: Array[String]) {
    val hello = new HelloWorld("Hello world!")
    hello.SayHello()
  }
}