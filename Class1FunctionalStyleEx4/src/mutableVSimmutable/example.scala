package mutableVSimmutable
/*
 * Scala is a hybrid functional language and pure object oriented language.
 * in functional language you cannot mutate a variable, but in scala you can.
 * Erlang is a good example of pure functional language.
 * 
 * Immutabilty vs Mutabilty
 * var -- indicates variable you can modify
 * val -- you cannot modify this, it is like final in C#
 * 
 * StringBuilder of java lang library is mutable.
 * 
 * value of buff1 is mutable, reference of buff1 is mutable
 * if you re assing buff1 with new StringBuilder, it does accept becasue buff1 is mutable
 * 
 * value of buff2 is still mutable, where as reference of buff2 is immutable now
 * so if you append something to buff2 it takes.
 * but if you try to 
 */
object example {
  def main(args: Array[String]) {
    //create a var buff1, 
    var buff1 = new StringBuilder
    buff1.append("hello")
    println(buff1)

    buff1 = new StringBuilder
    buff1.append("there")
    println(buff1)
  }
}