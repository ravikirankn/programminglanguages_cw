package totalvalues
/*
 * Scala is a statically typed
 * 
 * you have to put = symbol after the function def if u want scala to infer the return type of function.
 * 
 * you can send paramter list in scala
 * eg: foo(int a, double b)(int c, char d)
 * 
 * different ways of sending function as a pram to a function
 * 
 * values.foreach(println) // you are simply sending the pointer of function println to the foreach
 * values.foreach {println} // you are sending the function which calls println to the foreach
 * values.foreach{e=>println(e)} // this is same as above.
 * 
 * the difference between last two is:
 * in scala if you are sending something and recvng the same thing then you have a syntactically better
 * way of doing it.
 */
object totalValues {
  def totalValues(values: List[Int], selector: Int => Boolean) = {
    var total = 0
    for (e <- values) {
      if (selector(e)) total += e
    }
    total
  }
  def totalValuesIntrnlIterator(values: List[Int], selector: Int => Boolean) = {
    var total = 0
    values.foreach { e => if (selector(e)) total += e }
    total
  }
  def totalValuesParamList(values: List[Int])(selector: Int => Boolean) = {
    var total = 0
    values.foreach { e => if (selector(e)) total += e }
    total
  }
  def main(args: Array[String]) {
    val list = List(1, 2, 3, 4, 5, 6)
    println("Total of all values in list: " + totalValues(list, { e => true }))
    println("Total of all values in list using paramter list:" + totalValuesParamList(list) { e => true })
  }
}