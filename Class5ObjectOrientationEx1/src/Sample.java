/*
 * your can create only one instance of a single object.
 * 
 * How can you break singleton object created with — Reflection? Serialization ? Multi Threading ?
 * 
 * you may wanna use enum's if you really wanna implement Singleton correctly in java.
 * 
 * Scala handles it properly
 */
class Singleton {
  private static Singleton instance;

  private Singleton() {}

  public static Singleton getInstance() {
    if (instance == null) {
      instance = new Singleton();
    }
    return instance;
  }

  public static void setInstance(Singleton instance) {
    Singleton.instance = instance;
  }

}


public class Sample {
  public static void main(String[] args) {
    System.out.println(Singleton.getInstance());
    System.out.println(Singleton.getInstance()); // both will return same reference
  }
}
