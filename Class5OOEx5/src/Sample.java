import java.util.ArrayList;
import java.util.Collection;


public class Sample {

  public static void main(String[] args) {
    use1();
    use2();

    // this is a design flaw in java api.
    // remove on collections take the object.
    // remove on arraylist which is derived class of collection takes index.
    // see the same example in groovy it works correctly due to multi methods ? ?
  }

  public static void use1() {
    ArrayList<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(2);

    System.out.println("The size of list is : " + list.size());

    list.remove(0);

    System.out.println("The size of list is : " + list.size());
  }

  public static void use2() {
    Collection<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(2);

    System.out.println("The size of list is : " + list.size());

    list.remove(0);

    System.out.println("The size of list is : " + list.size());
  }

}
