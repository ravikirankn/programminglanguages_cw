import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebCrawler {
  static Logger LOGGER = Logger.getLogger(WebCrawler.class.getName());

  Set<String> setOfLinks = new HashSet<>();

  public Set<String> getLinksFromSite(String url) {
    LOGGER.debug("searching link -- " + url);
    try {
      Document doc = Jsoup.connect(url).get();
      Elements links = doc.select("a[href]");
      setOfLinks.add(url);
      for (Element link : links) {
        if (!StringUtil.isBlank(link.attr("abs:href")) && !setOfLinks.contains(link.attr("abs:href"))
            && new URI(url).getHost().equals(new URI(link.attr("abs:href")).getHost())) {
          getLinksFromSite(link.attr("abs:href"));
        }
      }
    } catch (IOException | URISyntaxException e) {
      LOGGER.error("caught exception: ", e);
    }
    return setOfLinks;
  }

}
