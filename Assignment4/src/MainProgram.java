import java.util.Arrays;
import java.util.List;

public class MainProgram {
  static String url = "http://www.cs.uh.edu";

  public static void main(String[] args) {
    System.out.println("---sequential solution---");
    List<?> stats = timeTaken(new WebCrawler());
    System.out.println("Number of sublinks in " + url + " : " + stats.get(0));
    System.out.println("Time taken : " + stats.get(1));
  }

  public static List<?> timeTaken(WebCrawler crawler) {
    long startTime = System.currentTimeMillis();
    int noOfLinks = crawler.getLinksFromSite(url).size();
    long endTime = System.currentTimeMillis();
    return Arrays.asList(noOfLinks, endTime - startTime);
  }
}
