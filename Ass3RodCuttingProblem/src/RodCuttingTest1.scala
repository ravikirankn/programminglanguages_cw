import java.util.ArrayList
import java.util.Collection
import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized.Parameters
import org.junit.runners.Parameterized
import scala.collection.immutable.List

@RunWith(value = classOf[Parameterized])
class RodCuttingTest1(expected: Int, length: Int, prices: List[Int]) {

  @Test def verifyRodCuttingRecursive {
    assertEquals(RodCutting.cutRodRecursive(prices, length), expected)
  }

  @Test def verifyTimeTakenRodCuttingRecursive {
    assertEquals(RodCutting.timeTaken(RodCutting.cutRodRecursive(prices, length))._1, expected)
  }

  @Test def verifyRodCuttingMemoized {
    assertEquals(RodCutting.cutRodMemoized(prices, length), expected)
  }

  @Test def verifyTimeTakenRodCuttingMemoized {
    assertEquals(RodCutting.timeTaken(RodCutting.cutRodMemoized(prices, length))._1, expected)
  }
}

object RodCuttingTest1 {
  @Parameters def parameters: Collection[Array[Any]] = {
    val list = new ArrayList[Array[Any]]()
    list.add(Array(0, 0, List()))
    list.add(Array(0, 0, List(1)))
    list.add(Array(0, 3, List(1)))
    list.add(Array(1, 1, List(1)))
    list.add(Array(0, -1, List(10, 21)))
    list.add(Array(0, 4, List(10, 21)))
    list.add(Array(10, 1, List(10, 21)))
    list.add(Array(21, 2, List(10, 21)))
    list.add(Array(0, 0, List(1, 5, 8, 9, 10, 17, 17, 20)))
    list.add(Array(10, 4, List(1, 5, 8, 9, 10, 17, 17, 20)))
    list.add(Array(17, 6, List(1, 5, 8, 9, 10, 17, 17, 20)))
    list.add(Array(18, 7, List(1, 5, 8, 9, 10, 17, 17, 20)))
    list.add(Array(22, 8, List(1, 5, 8, 9, 10, 17, 17, 20)))
    list
  }
}
