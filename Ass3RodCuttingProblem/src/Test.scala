
object Test {
  def strSqLen(s: String) = s.length * s.length
  var cache = new scala.collection.mutable.ListBuffer[Int]
  Stream.range(0, 8).foreach(i => cache += scala.Int.MinValue)

  def main(args: Array[String]) {
    val strSqLenMemoized = Memoize(strSqLen)
    println(strSqLenMemoized("hello Memo"))
    println(strSqLenMemoized("hello Memo"))
    println(strSqLen("hello Memo"))

    println(Stream.range(0, 5).foldLeft(scala.Int.MinValue)((x, y) =>
      {
        println(x, y)
        x + y
      }))

    val list = List(1, 2, 3, 4, 5, 6)
    list.updated(1, 100000)
    println("came here 1", list.updated(1, 100000))

    println(cache)

  }

  def cutRodMemoized(prices: List[Int], length: Int): Int = {
    val list = Stream.range(0, length + 1).map(i => scala.Int.MinValue).toList
    cutRodMemoizedAux(prices, length, list)
  }

  def cutRodMemoizedAux(prices: List[Int], length: Int, list: List[Int]): Int = {
    if (list(length) >= 0) {
      println("came here 2", list(length))
      list(length)
    }
    var maxValue = 0;
    if (length != 0) {
      maxValue = Stream.range(0, length).foldLeft(scala.Int.MinValue)((maxValue, i) =>
        scala.math.max(maxValue, prices(i) + cutRodMemoizedAux(prices, length - i - 1, list)))
    }
    list.updated(length, maxValue)
    println("came here 1", length, list)
    maxValue
  }

}