import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

object RodCutting {

  def cutRodRecursive(prices: List[Int], length: Int): Int = {
    length match {
      case x if x <= 0 || x > prices.length => 0
      case _ => {
        Stream.range(0, length / 2).foldLeft(List(prices(length - 1))) {
          (list, i) => list ::: List(cutRodRecursive(prices, i + 1) + cutRodRecursive(prices, length - i - 1))
        }.max
      }
    }
  }

  def timeTaken[T](block: => T): (T, Long) = {
    val timeBefore = System.currentTimeMillis
    val returnValueOfBlock = block
    val timeAfter = System.currentTimeMillis
    (returnValueOfBlock, timeAfter - timeBefore)
  }

  def printOutput[T](str: String)(output: (T, Long)) {
    println(str + output._2)
    println("Max obtained value: " + output._1)
  }

  var cache = new ListBuffer[Int]

  def cutRodMemoized(prices: List[Int], length: Int): Int = {
    Stream.range(0, length + 1).foreach(i => cache += scala.Int.MinValue)
    length match {
      case x if x <= 0 || x > prices.length => 0
      case _ => cutRodMemoizedAux(prices, length)
    }
  }

  private[this] def cutRodMemoizedAux(prices: List[Int], length: Int): Int = {
    if (cache(length) >= 0) cache(length)
    val maxValue = Stream.range(0, length / 2).foldLeft(List(prices(length - 1))) {
      (list, i) => list ::: List(cutRodMemoizedAux(prices, i + 1) + cutRodMemoizedAux(prices, length - i - 1))
    }.max
    cache.update(length, maxValue)
    maxValue
  }

  def main(args: Array[String]) {
    val length = 15;
    val prices = List(1, 5, 8, 9, 10, 17, 17, 20, 30, 40, 50, 60, 70, 80, 90)
    printOutput("Time taken for recursive algorithm (in msecs): ")(timeTaken(cutRodRecursive(prices, length)))
    printOutput("Time taken for memoized algorithm (in msecs): ")(timeTaken(cutRodMemoized(prices, length)))
  }
}