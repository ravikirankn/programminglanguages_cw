public static void use1() {
  ArrayList<Integer> list = new ArrayList<>();
  list.add(1);
  list.add(2);

  System.out.println("The size of list is : " + list.size());

  list.remove(0);

  System.out.println("The size of list is : " + list.size());
}

public static void use2() {
  Collection<Integer> list = new ArrayList<>();
  list.add(1);
  list.add(2);

  System.out.println("The size of list is : " + list.size());

  list.remove(0);

  System.out.println("The size of list is : " + list.size());
}

use1()
use2()
