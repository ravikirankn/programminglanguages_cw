package mixins

/*
 * Mixins and traits.
 * 
 * Multiple inheritance ?
 * Two problems with multiple inheritance
 * 1. we should deal with method collission.
 * 2. if its not an interface you are implementing, if you are extending from base classes. A is parent class for B and C.
 * If you wanna inherit from both B and C ? ? fill
 * 
 * trait is an interface with partially implemented methods.
 * you can place some constraints on traits
 * 
 * Scala supports traits, we can achieve at compile time
 * Ruby, Groovy supports mixins, we can achieve at run time.
 * 
 * 
 * you can mixin multiple traits as well.
 * in this example we mixin trait at class level and object level.
 */

trait Friend {
  val name: String
  def listen = println("I am your friend " + name + ", listening...")
}

class Human(val name: String) extends Friend {}
class Animal(val name: String)
class Dog(override val name: String) extends Animal(name) with Friend
class Cat(override val name: String) extends Animal(name)

class mixins {
  def talkToAFriends(friend: Friend) = {
    friend.listen
  }
  def main(args: Array[String]) {
    talkToAFriends(new Human("Peter"))
    talkToAFriends(new Dog("Snowy"))
    //mixin at object level
    val alf = new Cat("Alf")
    //talkToAFriends(alf)
    val yourCat = new Cat("YourCat") with Friend
    yourCat.listen
    talkToAFriends(yourCat)
  }
}