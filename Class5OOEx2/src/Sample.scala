/*
 * object keyword is used to create singleton in scala
 */

object Singleton {
  def op1() { println("op1....") }
}

class Sample {
  def main(args: Array[String]) {
    Singleton.op1();
  }
}