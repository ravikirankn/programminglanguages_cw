import scala.collection.mutable.ListBuffer
import scala.annotation.tailrec

object FibonacciSeries {
  def fibonacciRecursion(position: Int, listOfNumbers: ListBuffer[Long]): ListBuffer[Long] = {
    listOfNumbers.length match {
      case x if x >= position => return listOfNumbers
      case 0 | 1 => listOfNumbers += 1L
      case _ => listOfNumbers += listOfNumbers(listOfNumbers.length - 1) + listOfNumbers(listOfNumbers.length - 2)
    }
    fibonacciRecursion(position, listOfNumbers);
  }

  def fibonacciMutabilty(position: Int) = {
    var listOfNumbers: ListBuffer[Long] = ListBuffer()
    for (i <- 1 to position) {
      i match {
        case 1 | 2 => listOfNumbers += 1L
        case _ => listOfNumbers += listOfNumbers(i - 2) + listOfNumbers(i - 3)
      }
    }
    listOfNumbers
  }

  def fibonacciImmutabilty(position: Int) = {
    lazy val listOfNumbers: Stream[Int] = 1 #:: listOfNumbers.scanLeft(1)(_ + _)
    listOfNumbers.take(position).toList
  }

  @tailrec
  final def fibonacciTailRecursion(position: Int, listOfNumbers: List[Int] = List(1)): List[Int] = position - 1 match {
    case 0 => listOfNumbers
    case _ => fibonacciTailRecursion(position - 1, listOfNumbers :+ listOfNumbers.takeRight(2).sum)
  }

  def main(args: Array[String]): Unit = {
    println(fibonacciMutabilty(10))
    println(fibonacciImmutabilty(10))
    println(fibonacciTailRecursion(10))
  }
}