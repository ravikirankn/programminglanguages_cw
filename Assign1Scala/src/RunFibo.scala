
import scala.collection.mutable.ListBuffer

object RunFibo {
  def main(args: Array[String]) {
    println(FibonacciSeries.fibonacciMutabilty(10))
    println(FibonacciSeries.fibonacciImmutabilty(10))
    println(FibonacciSeries.fibonacciRecursion(2, ListBuffer()))

  }
}
