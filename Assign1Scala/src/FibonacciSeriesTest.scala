import java.util.ArrayList
import java.util.Collection
import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized.Parameters
import org.junit.runners.Parameterized

@RunWith(value = classOf[Parameterized])
class FibonacciSeriesTest(expected: Any, position: Any, entryList: Any) {
  @Test def verifyFiboMutabilty {
    assertEquals(FibonacciSeries.fibonacciMutabilty(position.asInstanceOf[Int]), expected)
  }

  @Test def verifyFiboRecursion {
    assertEquals(FibonacciSeries.fibonacciRecursion(position.asInstanceOf[Int], entryList.asInstanceOf[ListBuffer[Long]]), expected)
  }

  @Test def verifyFiboImutabilty {
    assertEquals(FibonacciSeries.fibonacciImmutabilty(position.asInstanceOf[Int]), expected)
  }
}

object FibonacciSeriesTest {
  @Parameters def parameters: Collection[Array[Any]] = {
    val list = new ArrayList[Array[Any]]()
    list.add(Array(ListBuffer(1, 1, 2, 3, 5, 8, 13, 21, 34, 55), 10, ListBuffer()))
    list.add(Array(ListBuffer(1), 1, ListBuffer()))
    list.add(Array(ListBuffer(1, 1), 2, ListBuffer()))
    list.add(Array(ListBuffer(), 0, ListBuffer()))
    list.add(Array(ListBuffer(), -1, ListBuffer()))
    list
  }
}