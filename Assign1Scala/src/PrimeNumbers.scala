object PrimeNumbers {
  def getPrimeNumbers(n: Int) = {
    Stream.range(2, n + 1).filter(x => isPrime(x)).toList
  }

  def isPrime(x: Int) = {
    if (Stream.range(2, x - 1).exists(e => x % e == 0)) false
    else true
  }

  def main(args: Array[String]) {
    println(getPrimeNumbers(10))
  }

}