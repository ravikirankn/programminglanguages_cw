public class Sample {
  public static void main(String[] args) {
    callCurrencyConverter(new CurrencyConvertor());
    callCurrencyConverter(new BetterCurrencyConvertor());
    callCurrencyConverter(new BetterCurrencyConvertor1());
  }

  public static void callCurrencyConverter(CurrencyConvertor convertor) {
    convertor.convert(5);
    // this is changed at compile time to convertor.convert(5.0);
    // postpone to runtime method call..
    // at runtime, examine the real object that converter refers to and call the
    // method convert that accepts double on it.
    // so in a statically typed language to get the correct functionality of polymorphism you should
    // override and overload the method at same time.
    //lets see how groovy handles it in next example
  }
}


class CurrencyConvertor {
  public void convert(double currency) {
    System.out.println("Inside CurrencyConvertor");
  }
}


class BetterCurrencyConvertor extends CurrencyConvertor {
  @Override
  public void convert(double currency) {
    System.out.println("Inside BetterCurrencyConvertor");
  }

  public void convert(int currency) {
    System.out.println("Inside BetterCurrencyConvertor");
  }
}


class BetterCurrencyConvertor1 extends CurrencyConvertor {
  public void convert(double currency) {
    System.out.println("Inside BetterCurrencyConvertor1");
  }
}
