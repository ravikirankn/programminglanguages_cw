public class Sample {
  /*
   * StringBuilder , Strings in Java String are immutable in java as we know. So advantages are its easy to share. When
   * we create strings across different places we are not exactly creating new instances we are sharing exactly the same
   * string instance. this is called interning of strings. benifits of interning is great sharing of memory and speed.
   */
  public static void main(String[] args) {
    String bad = new String("hello"); // don't do this. we loose the benifit of interning here.
    String greet = "hello";
    System.out.println(bad);
    System.out.println(greet);
    String greet2 = "hello";
    System.out.println(greet2);
    
    //greet and greet point to exactly the same instance in memory, where are bad is pointing to
    //a different instance of string in memory.
    
    System.out.println(greet.equals(bad)); // it says both are equal, reason being they are equal by value.
    System.out.println(greet==bad); //it does a reference based comparison so it returns false.
    System.out.println(greet==greet2); //it does a reference based comparison so it returns true.
    
    //interning 
    String internedString = bad.intern();
    System.out.println(greet == internedString); // this returns true
  }
}
