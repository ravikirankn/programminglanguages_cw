import java.util.ArrayList;
import java.util.List;

public class Sample {
  /*
   * pros and cons of static typing...
   * 
   * java is a statically typed language. conversion of types behind the scenes is done if the complier fees it is safe
   * to do so.
   * 
   * statically type languages lacks type safety sometimes. that is why we have generics, which we are not using the
   * below example. looks at below example. it fails at run time giving classcastexception
   * 
   * .net did a good job in implementing generics when compared to java. unknowingly.
   * 
   * java supports type irration. which messes up few things. watch oops method
   * 
   * even in better statically typed languages like scala you can end up into such kind of errors while using actors.
   */
  public static void oops(List list) {
    list.add(1.0);
  }

  public static void main(String[] args) {
    double value = 4.0;
    // value = "hello"; this will throw a compilation error
    value = 3;// even statically typed languages support some type of conversion between them.
              // behind the scene it will do type conversion.

    System.out.println(value);

    List<Integer> scores = new ArrayList<>();
    // List scores = new ArrayList();// withouht generics you can add 2.0 to list which will not be hanled at compile
    // time
    scores.add(1);
    scores.add(2);
    // scores.add(2.0); // you cannot put this if you use generics
    // oops(scores);
    int total = 0;
    for (Object e : scores) {
      total += (int) e;
    }
    System.out.println("total value is:" + total);

    List<Integer> items = new ArrayList<>();
    items.add(1);
    // items.add(1.0);
    items.add(2);
    //oops(items);
    int totalOfItems = 0;
    for (int item : items) {
      totalOfItems += item;
    }
    
    float x = 1.f / 81;
    System.out.println(x);
    double y = 1.0 / 81;
    System.out.println(y);
  }

}
