greaterThan3 number = number > 3

main = do
  let list1 = [1..10]
  let list2 = [1.1..5]
  let list3 = [1.6..5]
  let list4 = [2, 4..10]
  let list5 = [10..20]
  print(list1 ++ list5)
  print(list1)
  print(list5)
  print(0 : list1)
  print(head list1)
  print(tail list1)
  print(init list1)
  print(last list1)
  print(elem 2 list1)
  print(elem 21 list1)
  print(filter even list1)
  print(filter greaterThan3 list1)
  print(takeWhile even list1)
  print(takeWhile greaterThan3 list5)  
  print(dropWhile even list1)
  print(dropWhile greaterThan3 list1)
  print(dropWhile odd list1)
