public class Sample {
  /*
   * advantages of dynamic programming is it provides meta programming. if we say that program is dynamically typed and
   * so it is advantageous then there is no meaning in it. because its actually difficult and also in a sense
   * disadvantageous to program.
   * 
   * in java we can do meta programming with tool called aspectJ ?
   * 
   * the static nature of java limits us what we do at compile time. if you call some method on an object which is not
   * defined the compiler doesn't allow you to proceed. example show below greet.shout()
   * 
   * lets try the same example in groovy which runs on JVM itself.
   */
  public static void main(String[] args) {
    String greet = "hello";
    // greet.shout(); // no scope to call shout on greet
  }
}
