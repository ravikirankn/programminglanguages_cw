import scala.collection.mutable.Map

object companion {
  def main(args: Array[String]): Unit = {
    val blueBrush = Brush("Blue")
    val redBrush = Brush.apply("Red")
    val blueBrush2 = Brush("Blue")
    println(blueBrush.color)
    println(redBrush.color)
    println(blueBrush == blueBrush2)

  }
}

class Brush private (val color: String)

object Brush {
  val brushes: Map[String, Brush] = Map()
  def apply(color: String) = {
    if (!brushes.contains(color)) brushes(color) = new Brush(color)
    brushes(color)
  }
}

//this single ton is called companion object
//private constructor can be used by companion object brush but we can't use it outside it.