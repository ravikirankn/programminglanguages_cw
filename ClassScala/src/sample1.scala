
class IntUtil(number: Int) {
  def days = this
  def ago = {
    val today = java.util.Calendar.getInstance()
    today.add(java.util.Calendar.DAY_OF_MONTH, -number)
    today.getTime()
  }
}

object sample1 {

  implicit def convertIntToIntUitl(number: Int) = new IntUtil(number)

  def main(args: Array[String]): Unit = {
    val intUtil = new IntUtil(2)
    println(2.days.ago)
  }
}