import scala.math.BigInt

object partiallyAppliedFunctions {

  def main(args: Array[String]): Unit = {
    println(square(3))
    println(square(5))
  }

  def compute(number: Int, pow1: Int) = {
    BigInt(number) pow pow1
  }

  def square = compute(_: Int, 2)
  // this is a partially applied function
  // square takes one integer as parameter.
  // when we apply patter matching or use receive in concurrency scala creates few partially applied functions under the hood and does the job for us.
  // in goorvy it is called currying
}
