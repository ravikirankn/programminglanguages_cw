object sample2 {
  def main(args: Array[String]): Unit = {
    for (i <- 1 to 5) {
      println(i)
    }
    for (i <- new scala.runtime.RichInt(1).to(5)) {
      println(i)
    }
  }
}