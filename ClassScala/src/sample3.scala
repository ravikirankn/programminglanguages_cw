object sample3 {
  def main(args: Array[String]): Unit = {
    val car = new Car(1915, 0)
    println("year: " + car.year + " miles : " + car.miles)
    car.drive(10)
    println("new miles: " + car.miles)
  }
}

class Car(val year: Int, var miles: Int) {
  println("creating car...")
  def drive(dist: Int) = {
    miles += dist
  }
}
//what scala does is , it creates a final Int for year to which we cannot write, 
//It creates an Int miles to which we can read and wrtie.
//It creates a getter for year.
//It creates a getter and setter for miles.
//It creates a constructor for the class Car
//Whatever we put within the class is run as part of the constructor