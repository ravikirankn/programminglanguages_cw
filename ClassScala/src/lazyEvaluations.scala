object lazyEvaluations {

  def main(args: Array[String]): Unit = {
    println(foo(true))
    println(foo(false))
  }

  def fn = {
    println("In fn")
    5
  }

  def foo(flag: Boolean) = {
    lazy val somethng = fn
    println("I am in foo")
    if (flag) somethng * 2 else 11
  }
}
