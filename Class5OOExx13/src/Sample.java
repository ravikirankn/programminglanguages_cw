/*
 * You cannot change the argument of put method in BasketOfApples to Apple, then it's not
 * actually overriding the method.
 * 
 * we have to acheive it by checking instance of...
 * 
 * Scala can handle this problem in a better way see the next example.
 * 
 */

abstract class Fruit {}
class Apple extends Fruit {}
class Orange extends Fruit {}

abstract class Basket {
  void put(Fruit fruit) {
    System.out.println("put a fruit in basket..");
  }
}

class BasketOfApples extends Basket {
  @Override
  void put(Fruit fruit) {
    if(fruit instanceof Apple){
      System.out.println("put a fruit into basket of apples..");
    }else{
      throw new IllegalArgumentException("Invalid fruit passed...expecting apple..");
    }
    
  }
}

class BasketOfOranges extends Basket {
  @Override
  void put(Fruit fruit) {
    System.out.println("put a fruit into basket of oranges..");
  }
}

public class Sample {
  public static void putFruit(Basket basket, Fruit fruit){
    basket.put(fruit);
  }
  public static void main(String[] args) {
    putFruit(new BasketOfApples(), new Orange());
    putFruit(new BasketOfOranges(), new Orange());
  }
}
