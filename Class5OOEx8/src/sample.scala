/*
 * Execute around method pattern.
 * 
 * This is already available in java and C#
 * 
 * JAVA
 * synchronized(this){
 * 
 * }
 * 
 * 
 * C#
 * lock{
 * 
 * }
 * 
 * next example you will see how to do this ruby
 * 
 */

class Resource private {

  println("creating resource...")

  private def close() {
    println("closing resource...")
  }

  def op1 { println("op1....") }
  def op2 { println("op2....") }
  def op3 { throw new Exception }

}

object Resource {
  def use(block: Resource => Unit) {
    val res = new Resource
    try {
      block(res)
    } finally {
      res.close()
    }
  }
}

object sample {
  def main(args: Array[String]) {
    Resource.use {
      resource =>
        resource.op1
        resource.op2
        resource.op3
    }
  }
}