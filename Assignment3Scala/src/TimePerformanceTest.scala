import org.scalatest.FunSuite
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.Matchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TimePerformanceTest extends FunSuite with Matchers with TableDrivenPropertyChecks {

  val testsForTimeTaken = Table(
    ("length", "prices"),
    (15, List(0, 1, 3, 4, 5, 8, 9, 11, 12, 14, 15, 15, 16, 18, 19, 15, 20, 21)),
    (27, List(0, 1, 3, 4, 5, 8, 9, 11, 12, 14, 15, 15, 16, 18, 19, 15, 20, 21, 22, 24, 25, 24, 26, 28, 29, 35, 37, 38, 39, 40)))

  for ((length, prices) <- testsForTimeTaken) {
    test("test time taken for length: " + length + " prices: " + prices) {
      var rodCutting = new RodCutting()
      var rodCuttingMemo = new MemoizedRodCutting
      val rodCutR = rodCutting.timeTaken(rodCutting.cutRod(prices, length))
      val rodCutM = rodCutting.timeTaken(rodCuttingMemo.cutRod(prices, length))
      rodCutR._1 should equal(rodCutM._1)
      rodCutR._2 should be >= 10 * rodCutM._2
    }
  }
}