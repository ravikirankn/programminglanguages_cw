import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class RodCutting {

  def cutRod(prices: List[Int], length: Int): Int = {
    length match {
      case x if x <= 0 || x > prices.length => 0
      case _ => {
        Stream.range(0, length / 2).foldLeft(List(prices(length - 1))) {
          ((
            list, i) => list ::: List(cutRod(prices, i + 1) + cutRod(prices, length - i - 1)))
        }.max
      }
    }
  }

  def timeTaken[T](block: => T): (T, Long) = {
    val timeBefore = System.currentTimeMillis
    val returnValueOfBlock = block
    val timeAfter = System.currentTimeMillis
    (returnValueOfBlock, timeAfter - timeBefore)
  }
}