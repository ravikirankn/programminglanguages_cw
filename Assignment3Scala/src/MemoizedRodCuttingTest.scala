import org.scalatest.FunSuite
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.Matchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MemoizedRodCuttingTest extends FunSuite with Matchers with TableDrivenPropertyChecks {

  val testsForRodCut =
    Table(
      ("expected", "length", "prices"),
      (0, 0, List()),
      (0, 0, List(1)),
      (0, 3, List(1)),
      (1, 1, List(1)),
      (0, -1, List(10, 21)),
      (0, 4, List(10, 21)),
      (10, 1, List(10, 21)),
      (21, 2, List(10, 21)),
      (0, 0, List(1, 5, 8, 9, 10, 17, 17, 20)),
      (10, 4, List(1, 5, 8, 9, 10, 17, 17, 20)),
      (17, 6, List(1, 5, 8, 9, 10, 17, 17, 20)),
      (18, 7, List(1, 5, 8, 9, 10, 17, 17, 20)),
      (22, 8, List(1, 5, 8, 9, 10, 17, 17, 20)))

  for ((expected, length, prices) <- testsForRodCut) {
    test("test rod cut memoized test for prices: " + prices + " and length: " + length) {
      var rodCuttingMemo = new MemoizedRodCutting
      rodCuttingMemo.cutRod(prices, length) should equal(expected)
    }
  }

}