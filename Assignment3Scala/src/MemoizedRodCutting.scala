import scala.collection.mutable.Map

class MemoizedRodCutting extends RodCutting {

  var cache = Map.empty[Int, Int]

  override def cutRod(prices: List[Int], length: Int): Int = {
    if (!cache.contains(length)) {
      cache.put(length, super.cutRod(prices, length))
    }
    cache.get(length).get
  }

}