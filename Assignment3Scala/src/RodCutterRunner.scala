object RodCutterRunner {

  def printOutput[T](str: String)(output: (T, Long)) {
    println(str + output._2)
    println("Max obtained value: " + output._1)
  }

  def main(args: Array[String]) {
    val length = 27;
    val prices = List(0, 1, 3, 4, 5, 8, 9, 11, 12, 14,
      15, 15, 16, 18, 19, 15, 20, 21, 22, 24,
      25, 24, 26, 28, 29, 35, 37, 38, 39, 40)
    val map = Map[Int,Int]()
    var rodCutting = new RodCutting()
    var rodCuttingMemo = new MemoizedRodCutting
    printOutput("Time taken for recursive algorithm (in msecs): ")(rodCutting.timeTaken(rodCutting.cutRod(prices, length)))
    printOutput("Time taken for memoized algorithm (in msecs): ")(rodCutting.timeTaken(rodCuttingMemo.cutRod(prices, length)))
  }
}