package functionalstyle;

import java.util.ArrayList;
import java.util.List;

/*
 * In Functional type of programming:
 * 
 * 1. Functions are first class citizens. (We can decompose our application using functions not 
 * objects)
 * In Object oriented language we focus on creating objects which encapsulate data and behavior
 * so we treat objects are first class citizens
 * 
 * 2. Functions are higher order.
 * just like how we create objects in functions or return objects from functions in OOP.
 * here we can create functions in functions or return functions from functions.
 * 
 * 3. Functions don't have side effects.
 * what this means it they promote and maintain immutability.
 * in function programming we don't modify things, we create things.
 * 
 * Java is not a functional programming language but we can write a immutable code which accounts to
 * third part of the above.
 * 
 * we will try to solve the same problem of adding values without using mutable variable.
 * 
 */

public class TotalValuesImmutable {
	public static void main(String[] args) {
		List<Integer> values = new ArrayList<>();
		values.add(1);
		values.add(2);
		values.add(3);

		System.out
				.println(totalValuesWithImmutabilty(values, values.size(), 0));
	}

	private static int totalValuesWithImmutabilty(List<Integer> values,
			int size, int total) {
		if (size == 0)
			return total;
		return totalValuesWithImmutabilty(values, size - 1,
				total + values.get(size - 1));
	}
}
