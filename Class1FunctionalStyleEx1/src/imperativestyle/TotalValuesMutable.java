package imperativestyle;

import java.util.ArrayList;
import java.util.List;

/*
 * Imperative type of programming is prominent in languages such as c++, java, C#.
 * Even though these are OOPLS, The way we express out code to accomplish our job is quite imperative.
 * In imperative style we specify how to do things step by step, sequence of steps.
 * One more nature is to deal with mutabilty, it means we create a variable and try to modify it over,
 * and over again.
 * Here is an example which explains it.
 * Total collection of values and return back the result.
 * in this code we are modify total variable while looping through the list
 */
public class TotalValuesMutable {
	public static void main(String[] args) {
		List<Integer> values = new ArrayList<>();
		values.add(1);
		values.add(2);
		values.add(3);

		System.out.println(totalValues(values));
	}

	private static int totalValues(List<Integer> values) {
		int total = 0;
		// External iterator
		for (int value : values) {
			total += value;
		}
		return total;
	}
}
