object sample {
  def process(msg: Any) = {
    msg match {
      case 5 => println("high five!")
      case x : Int => println("I got an Int: "+x)
      case _ => println("Whatever")
    }
  }
  def main(args: Array[String]) {
    process(5)
    process(10)
    process(5.2)
    process("hello")
  }
}
