package class2
import scala.actors._
import Actor._

object actorsExample {

  def method1(number: Int) {
    for (i <- 1 to 3) {
      receive {
        case _ => println("in method1 with " + number + " thread " + Thread.currentThread)
      }
    }
  }

  def method2(number: Int) {
    loop {
      react {
        case _ => println("in method2 with " + number + " thread " + Thread.currentThread)
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val aktors = List(actor { method1(1) }, actor { method1(2) }, actor { method2(3) }, actor { method2(4) })
    for (i <- 1 to 3) {
      aktors.foreach { aktor => aktor ! "blah" }
      Thread.sleep(2000)
    }
  }

}