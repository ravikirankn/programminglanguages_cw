package class2

import scala.actors._
import Actor._

object Sample2 {

  def isPrime(number: Int): Boolean = {
    if (number == 1) return false
    for (i <- 2 to scala.math.sqrt(number).toInt) {
      if (number % i == 0) return false
    }
    return true
  }

  def countPrimes(lower: Int, upper: Int) = {
    var count = 0
    for (i <- lower to upper) if (isPrime(i)) count += 1
    count
  }

  def timeTaken[T](block: => T): (T, Double) = {
    val timeBefore = System.nanoTime
    val returnValueOfBlock = block
    val timeAfter = System.nanoTime
    (returnValueOfBlock, (timeAfter - timeBefore) / 1.0e9)
  }

  def countPrimesActorBased() = {
    val caller = self
    for (i <- 0 until 1000) {
      val lower = i * 1000 + 1
      val upper = lower + 999
      actor { caller ! countPrimes(lower, upper) }
    }
    var count = 0
    for (i <- 0 until 1000) {
      receiveWithin(3000) {
        case TIMEOUT =>
        case num: Int => count = count + num
      }
    }
    count
  }

  def main(args: Array[String]): Unit = {
    val seq = timeTaken(countPrimes(1, 1000000))
    println("# of primes inbtwn 1 to 1000000: " + seq._1)
    println("time taken: " + seq._2)
    val actorBased = timeTaken(countPrimesActorBased())
    println("# of primes inbtwn 1 to 1000000(actor based): " + actorBased._1)
    println("time taken: " + actorBased._2)
  }

}
