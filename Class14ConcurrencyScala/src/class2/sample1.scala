package class2

import scala.actors._
import Actor._

object sample1 {

  def getYearEndClosing(symbol: String, year: Int) = {
    val url = "http://ichart.yahoo.com/table.csv?s=" + symbol + "&a=10&b=01&c=" + year + "&d=10&e=31&f=" + year + "&g=d"
    val data = io.Source.fromURL(url).mkString
    val price = data.split("\n")(1).split(",")(4).toDouble
    (symbol, price)
  }

  def timeTaken[T](block: => T): (T, Double) = {
    val timeBefore = System.nanoTime
    val returnValueOfBlock = block
    val timeAfter = System.nanoTime
    (returnValueOfBlock, (timeAfter - timeBefore) / 1.0e9)
  }

  def sequential(symbols: List[String]) = {
    symbols.foldLeft(("", 0.0)) {
      (top, symbol) =>
        {
          val (sym: String, price: Double) = getYearEndClosing(symbol, 2014)
          if (price > top._2)
            (sym, price)
          else
            top
        }
    }
  }

  def actorBased(symbols: List[String]) = {
    val caller = self
    symbols.foreach(symbol => actor { caller ! getYearEndClosing(symbol, 2014) })
    symbols.foldLeft(("", 0.0)) {
      (top, symbol) =>
        {
          receive {
            case (sym: String, price: Double) =>
              if (price > top._2)
                (sym, price)
              else
                top
          }
        }
    }

  }

  def main(args: Array[String]): Unit = {

    val symbols = List("AAPL", "GOOG", "IBM", "ORCL", "MSFT")
    val topStockSequential = timeTaken(sequential(symbols))
    println("Top stock is " + topStockSequential._1._1 + " at price " + topStockSequential._1._2)
    println("Time taken " + topStockSequential._2)

    val topStockActorBased = timeTaken(sequential(symbols))
    println("Top stock is " + topStockActorBased._1._1 + " at price " + topStockActorBased._1._2)
    println("Time taken " + topStockActorBased._2)

  }

}