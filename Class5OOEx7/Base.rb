class Base
  def foo(value)
    value+2
  end
end

class Derived < Base
  def foo(*values)
    values.size()
  end
end

def use(inst)
  puts inst.foo(4)
end

use(Base.new)
use(Derived.new)

=begin
there is no way that you can accesss foo method of base class when you instantiate derived class, since polymorphysm has kicked in
=end