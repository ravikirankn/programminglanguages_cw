class Car
  attr_accessor:color
  attr_reader:miles
  attr_reader:year
  attr_reader :tire_pressure
  def initialize(year,color)
    @year = year
    @color = color
    @miles = 0
  end
  def drive(distance)
    @miles += distance
  end
  private :drive
end

car = Car.new(1994,"Black")
car.drive(10)
puts car.inspect

=begin
output of car.inspect
#<Car:0x007fe53508a350 @color="Black">

@ represent instance variable @@ represents class variable

ruby doesn't have function overloading, therefore ruby have only one constructor.
isn't it a limitation
not really, because ruby functions can receive multiple number of arguments

private is object specific but not class specific in ruby.
we can have a varibale 

=end