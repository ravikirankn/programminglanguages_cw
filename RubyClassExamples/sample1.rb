name = "Bob"
puts 'hello #{name}'
puts "hello #{name}"
puts "It's a nice weather outside today"
puts 'He said "That is awesome"'
puts 'He said "That''s awesome"'
puts %q/He said that's awesome/
puts %q!#{name} said that's awesome!
puts %Q!#{name} said that's awesome!

days_of_week = ["Sunday", 'Monday']
puts days_of_week
days_of_week = %w{Sunday Monday Tuesday Wednesday Thursday Friday Saturday}
puts days_of_week

memo = <<READ_UNTIL_YOU_SEE_THIS
This is an important information
blah blah blah
##This is called heredoc
READ_UNTIL_YOU_SEE_THIS

puts memo
=begin
Dynamic typing.
Fully object oriented.
Meta programming is the strength of ruby.
Strong conventions:
methods start with lower case and are separated with under score.
Class names start with upper case are mixed case
constants are all upper case separate with under under score

Strings in ruby are expressions.
to make use for that you need to enclose in double quotes and not single quotes. see the above example

=end