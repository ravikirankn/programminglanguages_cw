import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map

class RodCutting {

  def cutRod(prices: List[Int], length: Int): Int = {
    length match {
      case x if x <= 0 || x > prices.length => 0
      case _ => {
        Stream.range(0, length / 2).foldLeft(List(prices(length - 1))) {((
          list, i) => list ::: List(cutRod(prices, i + 1) + cutRod(prices, length - i - 1)))
        }.max
      }
    }
  }

  def timeTaken[T](block: => T): (T, Long) = {
    val timeBefore = System.currentTimeMillis
    val returnValueOfBlock = block
    val timeAfter = System.currentTimeMillis
    (returnValueOfBlock, timeAfter - timeBefore)
  }

  def printOutput[T](str: String)(output: (T, Long)) {
    println(str + output._2)
    println("Max obtained value: " + output._1)
  }

  def main(args: Array[String]) {
    val length = 15;
    val prices = List(1, 5, 8, 9, 10, 17, 17, 20, 30, 40, 50, 60, 70, 80, 90)
    printOutput("Time taken for recursive algorithm (in msecs): ")(timeTaken(cutRod(prices, length)))
    //printOutput("Time taken for memoized algorithm (in msecs): ")(timeTaken(cutRodMemoized(prices, length)))
  }
}