=begin

1. {|e| e - 1} // block or function value

2. 
  total=0
  {|e| total += e} // this is a closure because total has to be bounded or closed over to a variable outside
                      the scope.

3.why is it called closure and what is closure ?

total is not bound to any input parameter, so from where did total come into picture ?
a. e is bound to parameter.
b. total is bound to a variable in the scope of the caller of the function from where your block is called which
is each (function), so total is bound to the scope where the block is defined.

first we will take a look at the call stack.

level 1 is where total is defined.
level 2 is inside each method.
level 3 is inside the block.

From block (level 3 total is bound to variable in level 1)

so in order to bind the variable total, the scope is closed over the definition of the block itself.
because it closes over the scope, it is called a closure.

4.a purely functional language may not allow type 1 kind of operations in below examples.
because here we are modifying variable total ,and purely functional languages will not allow mutations.

5. we are also adding the values without using mutation, by using one of the internal iterators function 
(inject) 
first time when u call the iterator carryOver is bound to 0, and e is bound to 1.
  The function return 1 (carryOver + e)
second time inject calls the function again, this time carryOver is bound to 1 and e is bound to 2.
  The function returns 3
and so on...


=end

values = [1,2,3,4,5,6]
total = 0
values.each {|e| total += e}
puts "Total computed using mutable variable : #{total}"

puts "Total computed without using mutable variable: #{values.inject(0) {|carryOver, e| carryOver + e}}"

a = 1
puts a
a = "hello"
puts a
