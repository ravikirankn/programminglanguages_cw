=begin
1. It is hard to write functional style code in Java which is a OOPL as we saw it in previous
example  of totaling values.

2. Ruby is not a functional programming language, it is a dynamic language.

3. values.each { |e| puts e }

  we can notice that to each function we are passing some other function.
  so now what is a function?
  it has a name.
  it has body.
  it has parameters list.
  it has return type.
  in above code this is the function being passed to values.eacf
  {|e| puts e}
  1. puts e --  is body of the function
  2. |e| --  is parameter list, so it is taking single parameter e. ruby is a dynamic language and we don't
  have to specify the type of parameters.
  3. ruby being dynamic it automatically identifies the return type and we don't have to specify the 
  return type
  4. what's there in the name of the function? the function above doesn't have a name and it is called 
  anonymous function

4. In ruby everything is an expression, it return something for example each also return something.

5. We talked about external iterator in java in previous example in Java. In ruby we have something 
called internal iterator. each in the above example is an internal iterator.
we don't have to control the looping 

6. we will see what map function does on values.

  map will iterate on the values and return the a new collection after doing the operation which is specified
  inside it. puts values.map {|e| e * 2} will return 2,4,6,8,10,12.
  every function has an implicit return statement. The last statement in every function is returned irrespective
  of you return something, u can also do this |e| return e * 2 to perform same operation.
  map here collects each and every result that you return.

7. puts values.find {|e| e%2==0} will find the first even number in the list, u can used find_all to find all
even numbers. so find will collect the data if e%2==0 return true.

8. there are different types of internal iterator in ruby?


=end

values = [1,2,3,4,5,6]
#internal iterator
result = values.find_all { |e| e%2==0 }

for e in result
  print "#{e} , "
end
puts ""
for i in 0..result.count-1
  print "#{result[i]}"
  print " ," if i != result.count-1
  print " "
end
puts ""
puts result.join(", ")

x = 1 / 81.0
puts x