
callCurrencyConverter(new CurrencyConvertor())
callCurrencyConverter(new BetterCurrencyConvertor())
callCurrencyConverter(new BetterCurrencyConvertor1())

// this works as expected..
//groovy can provide both static typing and dynamic typing.
//the decision of what method should be called is postponed to run time


public static void callCurrencyConverter(CurrencyConvertor convertor) {
  convertor.convert(5);
}

class CurrencyConvertor {
  public void convert(double currency) {
    System.out.println("Inside CurrencyConvertor");
  }
}


class BetterCurrencyConvertor extends CurrencyConvertor {
  public void convert(int currency) {
    System.out.println("Inside BetterCurrencyConvertor");
  }
}


class BetterCurrencyConvertor1 extends CurrencyConvertor {
  public void convert(double currency) {
    System.out.println("Inside BetterCurrencyConvertor1");
  }
}