=begin
object chaining, minxins

in ruby class instances can have methods.
in below example.

alf ----> CatInstance ----> Cat(Metaclass)

when you call a method on cat
first it looks if the instance has a method,
if not it looks for the meta class if it has it.

but youCat is differet

youCat ---> virtualClass ----> CatInstance ----> Cat(meta class)

virtual class has mixin
if you have multiple mixins it will take precedence from right to left
include Friend, Minin1, Minx2
Friend methods will be called first, if its not found then Mixixn1 and so on...

=end

module Friend
  def listen()
    puts "I am "+name+" your friend, listening... "
  end
end

#mixin at class level
class Human
  include Friend
  attr_accessor :name
end

class Cat
  attr_accessor :name
end

peter = Human.new
peter.name = "Peter"
peter.listen

#mixins at object level
yourCat = Cat.new

class <<yourCat
  include Friend
end

yourCat.name = "Meow"
yourCat.listen

alf = Cat.new
alf.name = "Alf"
alf.listen