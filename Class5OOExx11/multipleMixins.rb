=begin
the order is right to left
or bottom to top
inst ---> vc Myclass ---> vc mixin1 ---> vc mixin2 ---> MyClass meta class
=end

module Mixin1
  def f1
    puts "Mixin1 f1 called.."
  end

  def g1
    puts "Mixin1 g1 called.."
  end
end

module Mixin2
  def f2
    puts "Mixin2 f2 called.."
  end

  def g1
    puts "Mixin2 g1 called.."
  end
end

class MyClass
  include Mixin1,Mixin2
  def g1
    puts "g1 of Myclass"
  end
end

inst = MyClass.new
inst.f1
inst.f2
inst.g1