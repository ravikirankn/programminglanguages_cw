public class Sample {

  private static boolean done;

  public static void main(String[] args) throws InterruptedException {
    new Thread(new Runnable() {
      @Override
      public void run() {
        int i = 0;
        while (!done) {
          i++;
        }
        System.out.println("Done!");
      }
    }).start();
    Thread.sleep(2000);
    done = true;
  }
}
