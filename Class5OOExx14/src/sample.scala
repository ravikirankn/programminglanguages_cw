/*
 * this examples shows us the usage of abstract types...which solves the problem in previous example.
 */

class Fruit {}
class Orange extends Fruit {}
class Apple extends Fruit {}

abstract class Basket {
  type Item <: Fruit
  def put(fruit: Item) {
    System.out.println("put a fruit in basket..");
  }
  def aFruitYouSupport: Item
}

class BasketOfOranges extends Basket {
  type Item = Orange
  override def put(fruit: Orange) {
    System.out.println("put a fruit into basket of oranges..");
  }
  def aFruitYouSupport: Item = new Orange
}

class BasketOfApples extends Basket {
  type Item = Apple
  override def put(fruit: Apple) {
    System.out.println("put a fruit into basket of apples..");
  }
  def aFruitYouSupport: Item = new Apple
}

object sample {

  def main(args: Array[String]) {
    putFruit(new BasketOfApples());
    putFruit(new BasketOfOranges());
  }

  def putFruit(basket: Basket) {
    basket.put(basket.aFruitYouSupport);
  }
}