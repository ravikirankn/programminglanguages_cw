import java.util.ArrayList;
import java.util.List;
class Animal {}
class Dog extends Animal {}
public class Sample {
  public static void playWithAnimal(List<Animal> animals) {}
  public static void copy(List<? extends Animal> animalsSrc, List<? extends Animal> animalsDesc) {}
  public static void main(String[] args) {
    playWithAnimal(new ArrayList<Animal>());
    copy(new ArrayList<Animal>(),new ArrayList<Animal>());
    copy(new ArrayList<Dog>(),new ArrayList<Dog>());
  }
}

/*
 *covariance , contravariance ? 
 */
