def hanoi_iterative(number_of_disks)
  a = number_of_disks.downto(1).to_a
  b = []
  c = []
  solution_states = []
  
  solution_states << [a, b, c]
  
  while !(solution_states.last[0].empty? && solution_states.last[1].empty?)
    solution_states << legal_move(solution_states.last, determine_move(number_of_disks, solution_states.length - 1))
  end
  solution_states
end

def hanoi_recursive(n, solution_states, start, spare, destination)
  if n < 1
    [[[],[],[]]]
  elsif n == 1
    new_solution_states1 = solution_states + [legal_move(solution_states.last, [start, destination])]
    new_solution_states1
  else
    new_solution_states2 = hanoi_recursive(n - 1, solution_states, start, destination, spare)
    
    new_solution_states3 = new_solution_states2 + [legal_move(new_solution_states2.last, [start, destination])]
    
    hanoi_recursive(n - 1, new_solution_states3, spare, start, destination)
  end
end

def hanoi_tail_recursive(solution_states, number_of_moves)
  if solution_states.last[0].empty? && solution_states.last[1].empty?
    solution_states
  else
    number_of_disks = solution_states.last.inject(0) { |sum, t| sum + t.length }
    move = determine_move(number_of_disks, number_of_moves)
    
    new_solution_states = solution_states + [legal_move(solution_states.last, move)]
  
    hanoi_tail_recursive(new_solution_states, number_of_moves + 1)
  end
end

def legal_move(state, towers_to_move)
  if state[towers_to_move[0]].empty?
    from = towers_to_move[1]
    to = towers_to_move[0]
    object_to_move = [state[towers_to_move[1]].last]
  elsif state[towers_to_move[1]].empty? || state[towers_to_move[0]].last < state[towers_to_move[1]].last
    from = towers_to_move[0]
    to = towers_to_move[1]
    object_to_move = [state[towers_to_move[0]].last]
  elsif state[towers_to_move[0]].last > state[towers_to_move[1]].last
    from = towers_to_move[1]
    to = towers_to_move[0]
    object_to_move = [state[towers_to_move[1]].last]
  end
  
  result_state =
    (0..2).collect do |tower|
      case tower
      when from
        state[from] - object_to_move 
      when to
        state[to] + object_to_move
      else
        state[tower]
      end
    end
  
  result_state
end

def determine_move(number_of_disks, number_of_moves)
  case
  when number_of_disks.even?
    if number_of_moves % 3 == 0
      [0, 1]
    elsif number_of_moves % 3 == 1
      [0, 2]
    else
      [1, 2]
    end
  when number_of_disks.odd?
    if number_of_moves % 3 == 0
      [0, 2]
    elsif number_of_moves % 3 == 1
      [0, 1]
    else
      [1, 2]
    end
  end
end

def print_state(state)
  state.each do |i|
    if !i.empty?
      print i*""+" "
    else
      print ". "
    end
  end
  puts
end