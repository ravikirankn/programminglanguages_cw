require_relative "../main/a2.rb"
require 'minitest/autorun'

class Hanoi_test < MiniTest::Unit::TestCase
  def setup
    @cases = [-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  end
  
  def test_hanoi_iterative
    @cases.each do |c|
      solution_states = hanoi_iterative(c)
      actual_moves = solution_states.length - 1
      c > -1 ? expected_moves = 2**c - 1 : expected_moves = 0
      
      assert_equal(expected_moves, actual_moves, "The number of moves is incorrect. Case #{c}")
      
      (1...solution_states.length).each do |move|
        assert(check_move(solution_states[move - 1], solution_states[move]), "Some moves are incorrect. Case #{c}")
      end
    end
  end
    
  def test_hanoi_recursive
    @cases.each do |c|
      initial_solution_state = [[c.downto(1).to_a, [], []]]
      solution_states =
        hanoi_recursive(c, initial_solution_state, 0, 1, 2)
      actual_moves = solution_states.length - 1
      c > -1 ? expected_moves = 2**c - 1 : expected_moves = 0
      
      assert_equal(expected_moves, actual_moves, "The number of moves is incorrect. Case #{c}")
      
      (1...solution_states.length).each do |move|
        assert(check_move(solution_states[move - 1], solution_states[move]), "Some moves are incorrect. Case #{c}")
      end
    end
  end
    
  def test_hanoi_tail_recursive
    @cases.each do |c|
      initial_solution_state = [[c.downto(1).to_a, [], []]]
      solution_states =
        hanoi_tail_recursive(initial_solution_state, 0)
      actual_moves = solution_states.length - 1
      c > -1 ? expected_moves = 2**c - 1 : expected_moves = 0
      
      assert_equal(expected_moves, actual_moves, "The number of moves is incorrect. Case #{c}")
      
      (1...solution_states.length).each do |move|
        assert(check_move(solution_states[move - 1], solution_states[move]), "Some moves are incorrect. Case #{c}")
      end
    end
  end
  
  def check_move(previous_state, state)
    valid = true
    top_previous =
      previous_state.collect { |t| t.empty? ? 0 : t.last }
    top_current = 
      state.collect { |t| t.empty? ? 0 : t.last }

    check1 = (0...state.length).collect { |i| top_current[i] - top_previous[i] }
    temp = (0...state.length).reject { |i| check1[i] == 0 }
      
    if temp.length == 2
      check2 = 
        case
        when top_current[temp[0]] - top_previous[temp[1]] == 0 && top_previous[temp[1]] != 0
         temp[0]
        when top_current[temp[1]] - top_previous[temp[0]] == 0 && top_previous[temp[0]] != 0
         temp[1]
        end
      valid = false if top_current[check2] > top_previous[check2] && top_previous[check2] != 0
    else
      valid = false
    end
    valid
  end
end