def getSubList(values,selector){
  values.findAll(selector)
}

def list1 = ["Jane","Jake","Bradley","Bill","Betty","Kara","Kris","Jil"]
def list2 = ["George","Kate","Karen","Kurt","Greg","Gary"]

println("------list1---")
println "Entire List: "+getSubList(list1) {true}
println "4 characters long: "+getSubList(list1) {it.length()==4}
println "3 characters long: "+getSubList(list1) {it.length()==3}
println "starts with K: "+getSubList(list1) {it.getAt(0).equals("K")}
println "starts with Z: "+getSubList(list1) {it.getAt(0).equals("Z")}
println("------list2---")
println "Entire List: "+getSubList(list2) {true}
println "4 characters long: "+getSubList(list2) {it.length()==4}
println "3 characters long: "+getSubList(list2) {it.length()==3}
println "starts with K: "+getSubList(list2) {it.getAt(0).equals("K")}
println "starts with Z: "+getSubList(list2) {it.getAt(0).equals("Z")}