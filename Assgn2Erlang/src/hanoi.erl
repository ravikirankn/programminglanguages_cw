#!/usr/bin/env escript

main(_) ->
	io:format("~nIterative moves: ~n"),
	print(hanoi_iterative1(2)).
	%io:format("~nRecursive moves: ~n"),
	%print(hanoi_recursive(5)),
	%io:format("~nTailRecursive moves: ~n"),
	%print(hanoi_tail_recursive(5)).

hanoi_recursive(N) when N > 0 ->
	Start = {0, lists:seq(1, N)},
	Spare = {1, []},
	Dest = {2, []},
	lists:last(hanoi_r(N, Start, Spare, Dest, [[Start, Spare, Dest]])).

hanoi_r(Peg, {A, [Peg| Start]}, {B, Spare}, {C, Dest}, Solution) when Peg > 0 ->
	Solution1 = Solution ++ [lists:keysort(1, [{A, Start}, {B, Spare}, {C, [Peg| Dest]}])],
	[{A, Start}, {B, Spare}, {C, [Peg| Dest]}, Solution1];
hanoi_r(Peg, Start, Spare, Dest, Solution) when Peg > 0 ->
	[Start1, Dest1, Spare1, Solution1] = hanoi_r(Peg - 1, Start, Dest, Spare, Solution),
	[Start2, Spare2, Dest2, Solution2] = hanoi_r(Peg, Start1, Spare1, Dest1, Solution1),
	[Spare3, Start3, Dest3, Solution3] = hanoi_r(Peg - 1, Spare2, Start2, Dest2, Solution2),
	[Start3, Spare3, Dest3, Solution3].


hanoi_tail_recursive(N) when N > 0 ->
	Start = {0, lists:seq(1, N)},
	Spare = {1, []},
	Dest = {2, []},
	hanoi_tr([Start, Spare, Dest], [[Start, Spare, Dest]], N, 0).

hanoi_tr([{_, []}, {_, []}, {_, _}], Solution, _, Moves) when Moves >= 0 ->
	Solution;
hanoi_tr([Start, Spare, Dest], Solution, Pegs, Moves) when Moves >= 0 ->
	[A, B] = determine(Pegs, Moves),
	C = 3 - A - B,
	{A, First} = lists:keyfind(A, 1, [Start, Spare, Dest]),
	{B, Second} = lists:keyfind(B, 1, [Start, Spare, Dest]),
	{C, Third} = lists:keyfind(C, 1, [Start, Spare, Dest]),
	[First1, Second1] = move({A, First}, {B, Second}),
	Solution1 = Solution ++ [lists:keysort(1, [First1, Second1, {C, Third}])],
	hanoi_tr(lists:last(Solution1), Solution1, Pegs, Moves + 1).


hanoi_iterative(N) when N > 0 ->
	Start = {0, lists:seq(1, N)},
	Spare = {1, []},
	Dest = {2, []},
	hanoi_i([[Start, Spare, Dest]], N, 0).
	
hanoi_i([[{A, []}, {B, []}, {C, L}]| T], _, _) ->
	lists:reverse([[{A, []}, {B, []}, {C, L}]| T]);
hanoi_i([H| T], Pegs, Moves) ->
	[A, B] = determine(Pegs, Moves),
	C = 3 - A - B,
	{A, First} = lists:keyfind(A, 1, H),
	{B, Second} = lists:keyfind(B, 1, H),
	{C, Third} = lists:keyfind(C, 1, H),
	[First1, Second1] = move({A, First}, {B, Second}),
	Result = lists:keysort(1, [First1, Second1, {C, Third}]),
	hanoi_i([Result| [H| T]], Pegs, Moves + 1).

hanoi_iterative1(N)->
	Start = {0, lists:seq(1, N)},
	Spare = {1, []},
	Dest = {2, []},
	[lists:foldl(fun(Moves, Input) -> hanoi_it(Input,N,Moves) end, [[Start, Spare, Dest]] , lists:seq(0, (1 bsl N)-1))].

hanoi_it([[{A, []}, {B, []}, {C, L}]| T], _, _) ->
	lists:reverse([[{A, []}, {B, []}, {C, L}]| T]);
hanoi_it([H| T], Pegs, Moves) ->
	[A, B] = determine(Pegs, Moves),
	C = 3 - A - B,
	{A, First} = lists:keyfind(A, 1, H),
	{B, Second} = lists:keyfind(B, 1, H),
	{C, Third} = lists:keyfind(C, 1, H),
	[First1, Second1] = move({A, First}, {B, Second}),
	Result = lists:keysort(1, [First1, Second1, {C, Third}]),
	[Result| [H| T]].

move({A, [H| T]}, {B, []}) ->
	[{A, T}, {B, [H]}];
move({A, []}, {B, [H| T]}) ->
	[{A, [H]}, {B, T}];
move({A, [H1|T1]}, {B, [H2| T2]}) ->
	if H1 < H2 -> [{A, T1}, {B, [H1| [H2| T2]]}];
		true -> [{A, [H2| [H1| T1]]}, {B, T2}]
	end.

determine(Pegs, Moves) ->
	case Moves rem 3 of
		0 -> [0, 1 + Pegs rem 2];
		1 -> [0, 2 - Pegs rem 2];
		2 -> [1, 2]
	end.

print({_, []}) ->
	io:format(". ");
print({_, P}) ->
	lists:foreach(fun(Peg) -> io:format("~p", [Peg]) end, lists:reverse(P)),
	io:format(" ");
print([{F, A}, {S, B}, {T, C}]) ->
	lists:foreach(fun(Tower) -> print(Tower) end, [{F, A}, {S, B}, {T, C}]),
	io:format("~n");
print(Solution) ->
	lists:foreach(fun(State) -> print(State) end, Solution).
