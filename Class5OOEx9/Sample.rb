class Resource
  puts "creating resource..."
  def op1
    puts "op1...."
  end

  def op2
    puts "op2...."
  end

  def op3
    raise "blah"
    puts "op3...."
  end

  def self.use
    resource = Resource.new
    begin
      yield resource
    ensure
      puts "cleaning resource..."
    end
  end

end

Resource.use do |resource|
  resource.op1
  resource.op2
  resource.op3
end
