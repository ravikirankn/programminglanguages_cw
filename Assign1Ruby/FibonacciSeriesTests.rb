require 'FibonacciSeries'
require 'param_test'
require 'test/unit'

class FibonacciSeriesTest < ActiveSupport::TestCase

  functions = [
    ['mutable', lambda { |position| fibonacci_mutability(position) }],
    ['immutability', lambda { |position| fibonacci_immutability(position) }],
    ['recursion', lambda { |position| fibonacci_recursion(position, []) }]]
  
  functions.each do |function|
    type, func = *function
    
    param_test "%s fibonacci #{type} position %s", [[Array[],-3],
      [Array[],0],
      [Array[1],1],
      [Array[1, 1, 2, 3, 5, 8, 13, 21, 34, 55],10],] do |expected, input|
      assert_equal expected, func.call(input)
    end
  end
end