def fibonacci_mutability(position)
  listOfNumbers = Array.new
  for i in 1..position
    case i
    when 1..2
      listOfNumbers.push(1)
    else
      listOfNumbers.push(listOfNumbers.at(i - 2) + listOfNumbers.at(i - 3))
    end
  end
  listOfNumbers
end

def fibonacci_recursion(position,listOfNumbers)
  case
  when listOfNumbers.length >= position
    return listOfNumbers
  when listOfNumbers.length == 0  || listOfNumbers.length == 1
    listOfNumbers.push(1)
  else
    listOfNumbers.push(listOfNumbers[-1] + listOfNumbers[-2])
  end
  fibonacci_recursion(position,listOfNumbers)
end

def fibonacci_immutability(position)
  (1..position).inject([]) { |array, x| x > 2 ? array + [array.last + array[-2]] : array + [1]}
end
