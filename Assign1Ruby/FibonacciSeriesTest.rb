require_relative "FibonacciSeries"
require "test/unit"

class FibonacciSeriesTest < Test::Unit::TestCase
  
  def test_fibonacci_mutability_PositionLessThan0
    assert_equal(Array[], fibonacci_mutability(-1) )
  end

  def test_fibonacci_mutability_Position0
    assert_equal(Array[], fibonacci_mutability(0) )
  end

  def test_fibonacci_mutability_Position1
    assert_equal(Array[1], fibonacci_mutability(1) )
  end

  def test_fibonacci_mutability_Position10
    assert_equal(Array[1, 1, 2, 3, 5, 8, 13, 21, 34, 55], fibonacci_mutability(10) )
  end

  def test_fibonacci_recursion_PositionLessThan0
    assert_equal(Array[], fibonacci_recursion(-1,Array.new) )
  end

  def test_fibonacci_recursion_Position0
    assert_equal(Array[], fibonacci_recursion(0,Array.new) )
  end

  def test_fibonacci_recursion_Position1
    assert_equal(Array[1], fibonacci_recursion(1,Array.new) )
  end

  def test_fibonacci_recursion_Position10
    assert_equal(Array[1, 1, 2, 3, 5, 8, 13, 21, 34, 55], fibonacci_recursion(10,Array.new) )
  end

  def test_fibonacci_immutability_PositionLessThan0
    assert_equal(Array[], fibonacci_immutability(-1) )
  end

  def test_fibonacci_immutability_Position0
    assert_equal(Array[], fibonacci_immutability(0) )
  end

  def test_fibonacci_immutability_Position1
    assert_equal(Array[1], fibonacci_immutability(1) )
  end

  def test_fibonacci_immutability_Position10
    assert_equal(Array[1, 1, 2, 3, 5, 8, 13, 21, 34, 55], fibonacci_immutability(10) )
  end
end