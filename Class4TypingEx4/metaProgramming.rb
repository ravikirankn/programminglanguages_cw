class Person
  def work
    puts "i am working..."
  end
=begin
  def method_missing(name,*args)
    puts "you called #{name}"
  end
=end
  def method_missing(name,*args)
    activities = ['Tennis','Football']
    activity =  name.to_s.split('play')[1]
    if(activities.include?(activity))
      puts "i am playing #{activity}"
    else
      puts "i don't play #{activity}"
    end
  end
end

sam=Person.new
sam.work

sam.playTennis
sam.playFootball
sam.playPolitics

=begin
sam.work prints i am working.
 sam.playTennis throws undefined method at runtime.

 just like groovy it didnt know what to do.

 so now we wanna support multiple methods, we writing something calle method_missing.
if something is not found this method is called. why?
=end