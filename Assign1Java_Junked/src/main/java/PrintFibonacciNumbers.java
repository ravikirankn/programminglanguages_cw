package main.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PrintFibonacciNumbers {

  public List<Long> fibonacciMutabilty(int position) {
    List<Long> listOfNumbers = new ArrayList<>();
    for (int i = 1; i <= position; i++) {
      if (i == 1 || i == 2)
        listOfNumbers.add(1L);
      else
        listOfNumbers.add(listOfNumbers.get(i - 2) + listOfNumbers.get(i - 3));
    }
    return listOfNumbers;
  }

  public List<Long> fibonacciRecursion1(int position) {
    return fibonacciRecursion(position, null);
  }

  public List<Long> fibonacciRecursion1(int position, List<Long> listOfNumbers) {
    if (position == 1) return new ArrayList<Long>(Arrays.asList(1L));
    if (position == 2) return new ArrayList<Long>(Arrays.asList(1L, 1L));
    listOfNumbers = fibonacciRecursion1(position - 1, listOfNumbers);
    listOfNumbers.add(listOfNumbers.get(position - 2) + listOfNumbers.get(position - 3));
    return listOfNumbers;
  }

  public List<Long> fibonacciRecursion(int position) {
    return fibonacciRecursion(position, null);
  }

  public List<Long> fibonacciRecursion(int position, List<Long> listOfNumbers) {
    listOfNumbers = new ArrayList<>();
    if (position > 0) {
      listOfNumbers.addAll(fibonacciRecursion(position - 1, listOfNumbers));
      if (position == 1 || position == 2)
        listOfNumbers = getNewList(listOfNumbers, 1L);
      else
        listOfNumbers = getNewList(listOfNumbers, listOfNumbers.get(position - 2) + listOfNumbers.get(position - 3));
    }
    return listOfNumbers;
  }

  public List<Long> fibonacciImmutabilty(int position) {
    List<Long> listOfNumbers = new ArrayList<>();
    for (int i = 1; i <= position; i++) {
      if (i == 1 || i == 2)
        listOfNumbers = getNewList(listOfNumbers, 1L);
      else
        listOfNumbers = getNewList(listOfNumbers, listOfNumbers.get(i - 2) + listOfNumbers.get(i - 3));
    }
    return listOfNumbers;
  }

  private List<Long> getNewList(List<Long> listOfNumbers, long l) {
    List<Long> newList = new ArrayList<>();
    newList.addAll(listOfNumbers);
    newList.add(l);
    return newList;
  }
}
