package test.java;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.java.PrintFibonacciNumbers;

import org.junit.Test;

public class PrintFibonacciNumbersTest {

  PrintFibonacciNumbers fibo = new PrintFibonacciNumbers();

  @Test
  public void testFiboMutabiltyWithPosistion1() {
    List<Long> list = fibo.fibonacciMutabilty(1);
    System.out.println("@Test -- fibonacciMutabilty {position = 1} : " + list);
    assertEquals(new ArrayList<Long>(Arrays.asList(1L)), list);
  }

  @Test
  public void testFiboMutabiltyWithPosistion10() {
    List<Long> list = fibo.fibonacciMutabilty(10);
    System.out.println("@Test -- fibonacciMutabilty {position = 10} : " + list);
    assertEquals(new ArrayList<Long>(Arrays.asList(1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L)), list);
  }

  @Test
  public void testFiboMutabiltyWithPosistion0() {
    List<Long> list = fibo.fibonacciMutabilty(0);
    System.out.println("@Test -- fibonacciMutabilty {position = 0} : " + list);
    assertEquals(new ArrayList<Long>(), list);
  }

  @Test
  public void testFiboMutabiltyWithPosistionLessThan0() {
    List<Long> list = fibo.fibonacciMutabilty(-10);
    System.out.println("@Test -- fibonacciMutabilty {position < 0} : " + list);
    assertEquals(new ArrayList<Long>(), list);
  }

  @Test
  public void testFiboRecursionWithPosistion1() {
    List<Long> list = fibo.fibonacciRecursion(1);
    System.out.println("@Test -- fibonacciRecursion {position = 1} : " + list);
    assertEquals(new ArrayList<Long>(Arrays.asList(1L)), list);
  }

  @Test
  public void testFiboRecursionWithPosistion10() {
    List<Long> list = fibo.fibonacciRecursion(10);
    System.out.println("@Test -- fibonacciRecursion {position = 10} : " + list);
    assertEquals(new ArrayList<Long>(Arrays.asList(1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L)), list);
  }

  @Test
  public void testFiboRecursionWithPosistion0() {
    List<Long> list = fibo.fibonacciRecursion(0);
    System.out.println("@Test -- fibonacciRecursion {position = 0} : " + list);
    assertEquals(new ArrayList<Long>(), list);
  }

  @Test
  public void testFiboRecursionWithPosistionLessThan0() {
    List<Long> list = fibo.fibonacciRecursion(-3);
    System.out.println("@Test -- fibonacciRecursion {position < 0} : " + list);
    assertEquals(new ArrayList<Long>(), list);
  }

  @Test
  public void testFiboImmutabiltyWithPosistion1() {
    List<Long> list = fibo.fibonacciImmutabilty(1);
    System.out.println("@Test -- fibonacciMutabilty {position = 1} : " + list);
    assertEquals(new ArrayList<Long>(Arrays.asList(1L)), list);
  }

  @Test
  public void testFiboImmutabiltyWithPosistion10() {
    List<Long> list = fibo.fibonacciImmutabilty(10);
    System.out.println("@Test -- fibonacciImmutabilty {position = 10} : " + list);
    assertEquals(new ArrayList<Long>(Arrays.asList(1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L)), list);
  }

  @Test
  public void testFiboImmutabiltyWithPosistion0() {
    List<Long> list = fibo.fibonacciImmutabilty(0);
    System.out.println("@Test -- fibonacciImmutabilty {position = 0} : " + list);
    assertEquals(new ArrayList<Long>(), list);
  }

  @Test
  public void testFiboImmutabiltyWithPosistionLessThan0() {
    List<Long> list = fibo.fibonacciImmutabilty(-6);
    System.out.println("@Test -- fibonacciImmutabilty {position < 0} : " + list);
    assertEquals(new ArrayList<Long>(), list);
  }
}
