x = 4
puts x
puts x.class

puts 'x'
puts 'x'.class

puts :x
puts :x.class

=begin

the out put is 

4       // value x is printed
Fixnum  //
x       // string x
String  //
x       // this is the variable x itself, this is a symbol or the reference to variable x itself
Symbol  //

we can used function references (symbols) for passing to functions
=end