import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class PrintFibonacciNumbersTest {

  PrintFibonacciNumbers fibo = new PrintFibonacciNumbers();

  private List<Long> expected;
  private int position;

  @Parameters
  public static Collection<Object[]> getTestParameters() {
    return Arrays.asList(new Object[][] { {new ArrayList<>(Arrays.asList(1L)), 1}, {new ArrayList<>(), 0},
        {new ArrayList<>(Arrays.asList(1L, 1L)), 2},
        {new ArrayList<Long>(Arrays.asList(1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L)), 10}, {new ArrayList<>(), -1}});
  }

  public void assertList(List<Long> list) {
    assertEquals(expected, list);
  }

  public PrintFibonacciNumbersTest(List<Long> expected, int position) {
    this.expected = expected;
    this.position = position;
  }

  @Test
  public void testFiboMutabilty() {
    assertList(fibo.fibonacciMutabilty(position));
  }

  @Test
  public void testFiboImmutabilty() {
    assertList(fibo.fibonacciImmutabilty(position));
  }

  @Test
  public void testFiboRecursion() {
    assertList(fibo.fibonacciRecursion(position,new ArrayList<Long>()));
  }
}
