/*
 * 2 * Copyright (c) 2013 Oracle and/or its affiliates. All rights reserved. 3 * 4 * Redistribution and use in source
 * and binary forms, with or without 5 * modification, are permitted provided that the following conditions 6 * are met:
 * 7 * 8 * - Redistributions of source code must retain the above copyright 9 * notice, this list of conditions and the
 * following disclaimer. 10 * 11 * - Redistributions in binary form must reproduce the above copyright 12 * notice, this
 * list of conditions and the following disclaimer in the 13 * documentation and/or other materials provided with the
 * distribution. 14 * 15 * - Neither the name of Oracle nor the names of its 16 * contributors may be used to endorse or
 * promote products derived 17 * from this software without specific prior written permission. 18 * 19 * THIS SOFTWARE
 * IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 20 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, 21 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 22 * PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 23 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, 24 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 25 * PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 26 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF 27 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 28 * NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS 29 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 30
 */
/*
 * 33 * This source code is provided to illustrate the usage of a given feature 34 * or technique and has been
 * deliberately simplified. Additional steps 35 * required for a production-quality application, such as security
 * checks, 36 * input validation and proper error handling, might not be present in 37 * this sample code. 38
 */
import java.util.stream.Stream;

/**
 * This demo shows how to use the parallel calculation to calculate Fibonacci sequence. This is a 2-dimensional system
 * of linear difference equations that describes the Fibonacci sequence. 47 * 48 * @author tyan 49
 */
public class Fibonacci {
  /**
   * 52 * First Fibonacci number. 53
   */
  private final static long FIBONACCI_1 = 1;

  /**
   * 57 * Second Fibonacci number. 58
   */
  private final static long FIBONACCI_2 = 1;

  /**
   * 62 * A base matrix that will be used to calculate Fibonacci sequence. 63
   */
  private final static long[][] BASE = new long[][] { {FIBONACCI_2, FIBONACCI_1}, {FIBONACCI_1, 0}};

  /**
   * @param args argument to run program
   */
  public static void main(String[] args) {
    try {
      int position = 10;
      if (position < 3) {
        throw new Exception("Postiion must be greater than 3");
      }
      long[][] fibo = power(BASE, position);
      for (int i = 0; i < fibo.length; i++) {
        for(int j = 0; j < fibo[i].length; j++){
          System.out.println(fibo[i][j]);
        }
      }
      // System.out.printf("The %dth fibonacci number is %d\n", position, power(BASE, position)[0][1]);
    } catch (Exception nfe) {
      usage();
    }
  }

  /**
   * 90 * Matrix binaries operation multiplication. 91 * @param matrix1 matrix to be multiplied. 92 * @param matrix2
   * matrix to multiply. 93 * @return A new generated matrix which has same number of rows as matrix1 94 * and same
   * number of columns as matrix2.
   */
  private static long[][] times(long[][] matrix1, long[][] matrix2) {
    long[][] result = new long[2][2];
    for (int row = 0; row < matrix1.length; row++) {
      for (int col = 0; col < matrix2[row].length; col++) {
        for (int col1 = 0; col1 < matrix1[row].length; col1++) {
          result[row][col] += matrix1[row][col1] * matrix2[col1][col];
        }
      }
    }
    return result;
  }

  /**
   * Power operation to matrix. Requirement for power operation is matrix must have same number row and column.
   * 
   * @param matrix base
   * @param n the exponent
   * @return the value of the first argument raised to the power of the second argument.
   */
  private static long[][] power(long[][] matrix, int n) {
    return Stream.generate(() -> matrix).limit(n).reduce(Fibonacci::times).get();
  }

  /**
   * Usage of this program
   */
  public static void usage() {
    System.out.println("Usage: java Fibonacci Position");
    System.out.println("Postiion must be a integer greater than 3");
  }
}
