import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.function.LongUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class MainClass {
  public static void main(String[] args) throws NoSuchMethodException, SecurityException {
    PrintFibonacciNumbers fibo = new PrintFibonacciNumbers();
    System.out.println("fibonacciMutabilty: " + fibo.fibonacciMutabilty(0));
    System.out.println("fibonacciRecursion: " + fibo.fibonacciRecursion(1, new ArrayList<Long>()));
    // int temp = Stream.iterate(1, i -> i + 1).limit(10).reduce(0, (x, y) -> {
    // int sum = x + y;
    //
    // System.out.println("Intermediate result for sum: " + sum + " x: " + x + " y: " + y);
    // return sum;
    // });
    // System.out.println(temp);
    // System.out.println(Stream
    // .iterate(new ArrayList<Long>(Arrays.asList(1L)), i -> new ArrayList<Long>(Arrays.asList(i.get(0) + 1)))
    // .limit(10).collect(Collectors.toList()));
    // Stream.iterate(new ArrayList<Long>(Arrays.asList(1L)), i -> new ArrayList<Long>(Arrays.asList(i.get(0) + 1)))
    // .limit(10).reduce(new ArrayList<Long>(Arrays.asList(1L)), (x, y) -> {
    // if (y.get(0) == 1) return x;
    // ArrayList<Long> list = new ArrayList<>();
    // System.out.println("Intermediate result: " + list + " x: " + x + " y: " + y);
    // return list;
    // });



    // Stream.iterate(1, i -> i + 1).limit(10)
  }

  LongUnaryOperator unaryOperator = null;

  public List<Long> fibonacciImmutabilty(int position) {
    return LongStream.range(1, position + 1).map(unaryOperator = i -> {
      return i == 1 || i == 2 ? 1L : unaryOperator.applyAsLong(i - 2) + unaryOperator.applyAsLong(i - 1);
    }).boxed().collect(Collectors.toList());
  }
}
