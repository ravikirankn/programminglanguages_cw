import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class PrintFibonacciNumbers {
  public List<Long> fibonacciMutabilty(int position) {
    List<Long> listOfNumbers = new ArrayList<>();
    for (int i = 1; i <= position; i++) {
      if (i == 1 || i == 2)
        listOfNumbers.add(1L);
      else
        listOfNumbers.add(listOfNumbers.get(i - 2) + listOfNumbers.get(i - 3));
    }
    return listOfNumbers;
  }

  public List<Long> fibonacciRecursion(int position, List<Long> listOfNumbers) {
    if (listOfNumbers.size() >= position)
      return listOfNumbers;
    else if (listOfNumbers.size() == 0 || listOfNumbers.size() == 1)
      listOfNumbers.add(1L);
    else if (listOfNumbers.size() > 1)
      listOfNumbers.add(listOfNumbers.get(listOfNumbers.size() - 1) + listOfNumbers.get(listOfNumbers.size() - 2));
    return fibonacciRecursion(position, listOfNumbers);
  }

  public ArrayList<Long> fibonacciImmutabilty(int position) {
    return position > 0
        ? Stream.iterate(1L, e -> e + 1L).limit(position)
            .reduce(new ArrayList<Long>(), (x, y) -> fibonacciAccumilator(x, y), (c1, c2) -> fibonacciCombiner(c1, c2))
        : new ArrayList<>();
  }

  private ArrayList<Long> fibonacciAccumilator(ArrayList<Long> newList, Long position) {
    if (position == 1 || position == 2)
      newList.add(1L);
    else
      newList.add(newList.get((int) (position - 2)) + newList.get((int) (position - 3)));
    return newList;
  }

  private ArrayList<Long> fibonacciCombiner(ArrayList<Long> newList, ArrayList<Long> oldList) {
    newList.addAll(oldList);
    return newList;
  }

}
