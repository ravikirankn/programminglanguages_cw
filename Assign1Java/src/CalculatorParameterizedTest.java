import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.assertEquals;

@RunWith(value = Parameterized.class)
public class CalculatorParameterizedTest {

  private double expected;
  private double valueOne;
  private double valueTwo;

  /**
   * @return
   */
  @Parameters
  public static Collection<Integer[]> getTestParameters() {
    return Arrays.asList(new Integer[][] { {2, 1, 1},
        // expected, valueOne, valueTwo
        {3, 2, 1}, // expected, valueOne, valueTwo
        {4, 3, 1}, // expected, valueOne, valueTwo
    });
  }

  /**
   * @param expected
   * @param valueOne
   * @param valueTwo
   */
  public CalculatorParameterizedTest(double expected, double valueOne, double valueTwo) {
    this.expected = expected;
    this.valueOne = valueOne;
    this.valueTwo = valueTwo;
  }

  /**
   * @see eg.com.tm.junit.source.Calculator#add(double,double)
   */
  @Test
  public void testAdd() {
    Calculator calc = new Calculator();
    assertEquals(expected, calc.add(valueOne, valueTwo), 0);
  }
}
