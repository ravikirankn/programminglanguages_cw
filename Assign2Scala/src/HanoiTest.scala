import java.util.ArrayList
import java.util.Collection
import org.junit.Assert._
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized.Parameters
import org.junit.runners.Parameterized
import org.hamcrest.core.IsNot
import org.hamcrest.core.IsEqual
import scala.collection.immutable.SortedMap

@RunWith(value = classOf[Parameterized])
class HanoiTest(expected: SortedMap[Int, List[List[Int]]], input: Int) {
  @Test def verifyHanoiIterative {
    assertThat(Hanoi.iterativeTowerOfHanoi(input), IsEqual.equalTo(expected))
  }

  @Test def verifyHanoiRecursive {
    assertThat(Hanoi.recursiveTowerOfHanoi(input), IsEqual.equalTo(expected))
  }

  @Test def verifyHanoiTailRecursive {
    assertThat(Hanoi.tailRecursiveTowerOfHanoi(input), IsEqual.equalTo(expected))
  }
}
object HanoiTest {
  @Parameters def parameters: Collection[Any] = {
    val list = new ArrayList[Any]()
    list.add(Array(SortedMap(0 -> List(List(1), List(), List()), 1 -> List(List(), List(), List(1))), 1))
    list.add(Array(SortedMap(0 -> List(List(1, 2), List(), List()), 1 -> List(List(2), List(1), List()), 2 -> List(List(), List(1), List(2)), 3 -> List(List(), List(), List(1, 2))), 2))
    list.add(Array(SortedMap(0 -> List(List(1, 2, 3, 4, 5), List(), List()), 1 -> List(List(2, 3, 4, 5), List(), List(1)), 2 -> List(List(3, 4, 5), List(2), List(1)), 3 -> List(List(3, 4, 5), List(1, 2), List()), 4 -> List(List(4, 5), List(1, 2), List(3)), 5 -> List(List(1, 4, 5), List(2), List(3)), 6 -> List(List(1, 4, 5), List(), List(2, 3)), 7 -> List(List(4, 5), List(), List(1, 2, 3)), 8 -> List(List(5), List(4), List(1, 2, 3)), 9 -> List(List(5), List(1, 4), List(2, 3)), 10 -> List(List(2, 5), List(1, 4), List(3)), 11 -> List(List(1, 2, 5), List(4), List(3)), 12 -> List(List(1, 2, 5), List(3, 4), List()), 13 -> List(List(2, 5), List(3, 4), List(1)), 14 -> List(List(5), List(2, 3, 4), List(1)), 15 -> List(List(5), List(1, 2, 3, 4), List()), 16 -> List(List(), List(1, 2, 3, 4), List(5)), 17 -> List(List(1), List(2, 3, 4), List(5)), 18 -> List(List(1), List(3, 4), List(2, 5)), 19 -> List(List(), List(3, 4), List(1, 2, 5)), 20 -> List(List(3), List(4), List(1, 2, 5)), 21 -> List(List(3), List(1, 4), List(2, 5)), 22 -> List(List(2, 3), List(1, 4), List(5)), 23 -> List(List(1, 2, 3), List(4), List(5)), 24 -> List(List(1, 2, 3), List(), List(4, 5)), 25 -> List(List(2, 3), List(), List(1, 4, 5)), 26 -> List(List(3), List(2), List(1, 4, 5)), 27 -> List(List(3), List(1, 2), List(4, 5)), 28 -> List(List(), List(1, 2), List(3, 4, 5)), 29 -> List(List(1), List(2), List(3, 4, 5)), 30 -> List(List(1), List(), List(2, 3, 4, 5)), 31 -> List(List(), List(), List(1, 2, 3, 4, 5))), 5))
    list
  }
}