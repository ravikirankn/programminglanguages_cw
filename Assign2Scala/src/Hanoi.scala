import scala.collection.immutable.SortedMap
import scala.annotation.tailrec

object Hanoi {

  def iterativeTowerOfHanoi(numberDisks: Int): SortedMap[Int, List[List[Int]]] = {
    val inputPegs = getInputPegs(numberDisks)
    Stream.range(0, (1 << numberDisks) - 1).foldLeft(inputPegs)((pegs, moveNumber) => {
      val disc = discToBeMoved(moveNumber);
      val source = (noOfMovements(moveNumber, disc) * direction(disc, numberDisks: Int)) % 3;
      val dest = (source + direction(disc, numberDisks: Int)) % 3;
      pegs ++ Map(moveNumber + 1 -> moveOneDisk(source, dest, pegs.get(moveNumber).get))
    })
  }

  def recursiveTowerOfHanoi(numberDisks: Int): SortedMap[Int, List[List[Int]]] = {
    moveStack(numberDisks, 0, 2, getInputPegs(numberDisks))
  }

  def tailRecursiveTowerOfHanoi(numberDisks: Int): SortedMap[Int, List[List[Int]]] = {
    tailRecursiveTowerOfHanoiHelper(numberDisks, 0, getInputPegs(numberDisks))
  }

  @tailrec
  def tailRecursiveTowerOfHanoiHelper(numberDisks: Int, numberOfMoves: Int, pegs: SortedMap[Int, List[List[Int]]]): SortedMap[Int, List[List[Int]]] = {
    if (pegs.last._2(0).isEmpty && pegs.last._2(1).isEmpty) pegs
    else {
      val disc = discToBeMoved(numberOfMoves);
      val source = (noOfMovements(numberOfMoves, disc) * direction(disc, numberDisks: Int)) % 3;
      val dest = (source + direction(disc, numberDisks: Int)) % 3;
      val updatePegs = pegs ++ Map(pegs.last._1 + 1 -> moveOneDisk(source, dest, pegs.last._2))
      tailRecursiveTowerOfHanoiHelper(numberDisks, numberOfMoves + 1, updatePegs)
    }
  }

  def moveStack(n: Int, from: Int, to: Int, pegs: SortedMap[Int, List[List[Int]]]): SortedMap[Int, List[List[Int]]] = {
    if (n == 1) {
      pegs ++ Map(pegs.last._1 + 1 -> moveOneDisk(from, to, pegs.last._2))
    } else {
      val other = 3 - from - to
      val pegs1 = moveStack(n - 1, from, other, pegs)
      val pegs2 = pegs1 ++ Map(pegs1.last._1 + 1 -> moveOneDisk(from, to, pegs1.last._2))
      moveStack(n - 1, other, to, pegs2)
    }
  }

  def moveOneDisk(from: Int, to: Int, pegs: List[List[Int]]): List[List[Int]] = {
    require(pegs(to).isEmpty || pegs(from).head < pegs(to).head)
    pegs.updated(to, pegs(from).head +: pegs(to)).updated(from, pegs(from).tail)
  }

  def discToBeMoved(moveNumber: Int): Int = {
    Stream.range(0, 5).foldLeft(Map(moveNumber + 1 -> 0))((moveDiscMap, discNumber) => {
      if (moveDiscMap.last._1 % 2 == 0) Map((moveDiscMap.last._1 / 2) -> (discNumber + 1))
      else moveDiscMap
    }).last._2
  }

  def noOfMovements(moveNumber: Int, disc: Int): Int = {
    ((moveNumber >> disc) + 1) >> 1;
  }

  def direction(disc: Int, numberDisks: Int): Int = {
    2 - (numberDisks + disc + 1) % 2;
  }

  def getInputPegs(numberDisks: Int): SortedMap[Int, List[List[Int]]] = {
    SortedMap(0 -> List((1 to numberDisks).toList, List[Int](), List[Int]()))
  }

  def main(args: Array[String]) {
    val numberDisks = 5;
    println("Iterative moves:")
    printCurrentState(iterativeTowerOfHanoi(numberDisks))
    println("Recursive moves:")
    printCurrentState(recursiveTowerOfHanoi(numberDisks))
    println("Tail Recursive moves:")
    printCurrentState(tailRecursiveTowerOfHanoi(numberDisks))
  }

  def printCurrentState(pegs: SortedMap[Int, List[List[Int]]]) {
    println(pegs.keys.foreach(i => {
      pegs(i).foreach(peg => {
        if (peg.nonEmpty) peg.reverse.foreach(print)
        else print(".")
        print(" ")
      })
      println
    }))
  }
}