import scala.collection.immutable

object Hanoi1 {

  def check1(data: Any) = {
    data match {
      case 5 => println("got 5")
      case 10 => println("got 10")
    }
  }

  def main(args: Array[String]) {
    check1(10)
    check1(5)
    //check("heloo")
    //println(pf.isDefinedAt("hello"))
    try {
      check1(0) //
    } catch {
      case ex: Exception => println(ex)
    }
  }
}
