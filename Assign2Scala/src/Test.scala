import scala.annotation.tailrec

object Test {
  def main(args: Array[String]) {
    println(recursionFactorial(500000))
    //println(tailrecursionFactorial(0, 1, 5000000))
  }
  
  def recursionFactorial(number: Int): Int = {
    if (number == 1) {
      return 1;
    }
    return number * recursionFactorial(number - 1);
  }

  def tailrecursionFactorial(a: Int, b: Int, count: Int): Int = {
    if (count == 0) {
      return b;
    }
    return tailrecursionFactorial(a + 1, (a + 1) * b, count - 1);
  }

}