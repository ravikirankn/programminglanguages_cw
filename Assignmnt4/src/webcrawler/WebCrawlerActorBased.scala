package webcrawler

import scala.actors._
import Actor._
import java.util.function.Consumer
import org.jsoup.nodes.Element
import scala.collection.mutable.Set

class WebCrawlerActorBased extends WebCrawler {

  override def getLinksFromSite(url: String, fromSiteFlag: Boolean = true): Set[String] = {
    val links = getSubUrlsFromUrl(url, fromSiteFlag)
    //println(setOfLinks)
    receiveAndAdd()
    links.forEach(new Consumer[Element]() {
      def accept(href: Element) {
        val subUrl = getSubUrl(href, fromSiteFlag)
        if (!subUrl.isEmpty() && !setOfLinks.contains(subUrl) && breakingCondition(url, subUrl, fromSiteFlag)) {
          //println(subUrl)
          actor { getLinksFromSite(subUrl, fromSiteFlag) };
        }
      }
    })
    setOfLinks
  }

  def receiveAndAdd() {
    if (setOfLinks.size != 0) {
      loop {
        receive {
          case _ => println("am here")
          case links: Set[_] => {
            println(links)
            setOfLinks.++(links)
          }
          case TIMEOUT => println("timed out")
        }
      }
    }
  }
}