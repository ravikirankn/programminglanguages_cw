package webcrawler

import java.io.File
import java.net.URI
import java.net.URISyntaxException
import java.util.function.Consumer
import scala.collection.mutable.Set
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

class WebCrawler {

  val setOfLinks = Set[String]()

  def getLinksFromSite(url: String, fromSiteFlag: Boolean = true): Set[String] = {
    val links = getSubUrlsFromUrl(url, fromSiteFlag)
    setOfLinks.add(url)
    links.forEach(new Consumer[Element]() {
      def accept(href: Element) {
        val subUrl = getSubUrl(href, fromSiteFlag)
        if (!subUrl.isEmpty() && !setOfLinks.contains(subUrl) && breakingCondition(url, subUrl, fromSiteFlag)) {
          getLinksFromSite(subUrl, fromSiteFlag);
        }
      }
    })
    setOfLinks
  }

  def breakingCondition(url: String, subUrl: String, fromSiteFlag: Boolean) = {
    !fromSiteFlag || new URI(url).getHost().equals(new URI(subUrl).getHost())
  }

  def getSubUrl(element: Element, fromSiteFlag: Boolean): String = {
    if (fromSiteFlag) {
      element.attr("abs:href")
    } else {
      element.attr("href")
    }
  }

  def getSubUrlsFromUrl(url: String, fromSiteFlag: Boolean): Elements = {
    if (fromSiteFlag) {
      val response = Jsoup.connect(url).ignoreContentType(true).ignoreHttpErrors(true).execute();
      if (response.statusCode() == 200) response.parse().select("a[href]")
      else new Elements
    } else {
      val doc = Jsoup.parse(new File(url), "UTF-8")
      doc.select("a[href]");
    }
  }

}