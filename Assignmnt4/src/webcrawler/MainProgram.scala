package webcrawler

object MainProgram {

  def main(args: Array[String]) {
    val crawler = new WebCrawler
    val crawlerActorBased = new WebCrawlerActorBased
    val url = "http://www2.cs.uh.edu/~svenkat/fall2014pl/samplepages"

    println(s"Start url: $url")
    lazy val stats = timeTaken { crawler.getLinksFromSite(url) }
    //println(s"Sequential: ${stats._1.size} links, took ${stats._2} msecs")

    lazy val stats1 = timeTaken { crawlerActorBased.getLinksFromSite(url) }
    println(s"Actor Based: ${stats1._1.size} links, took ${stats1._2} msecs")

  }

  def timeTaken[T](block: => T): (T, Long) = {
    val timeBefore = System.currentTimeMillis
    val returnValueOfBlock = block
    val timeAfter = System.currentTimeMillis
    (returnValueOfBlock, timeAfter - timeBefore)
  }

}