package webcrawler

import org.scalatest.FunSuite
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.Matchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import java.io.FileNotFoundException
import java.net.UnknownHostException

@RunWith(classOf[JUnitRunner])
class WebCrawlerTest extends FunSuite with Matchers with TableDrivenPropertyChecks {

  test("getLinksFromSite for url: index.html") {
    val crawler = new WebCrawler();
    crawler.getLinksFromSite("index.html", false).size should be(5)
  }

  test("getLinksFromSite for invalidLink") {
    val crawler = new WebCrawler();
    an[FileNotFoundException] should be thrownBy { crawler.getLinksFromSite("invalidLink", false) }
  }

  test("getLinksFromSite for unknowHost") {
    val crawler = new WebCrawler();
    an[UnknownHostException] should be thrownBy { crawler.getLinksFromSite("http://unknowHost.comasd") }
  }
}